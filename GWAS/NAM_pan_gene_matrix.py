import os
from os import listdir
from os.path import isfile, join
import pickle

mypath = "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/Pairwise_pan_gene_pair/"


class GeneClusters:
    def __init__(self):
        self.genes = set()

    def __hash__(self):
        hash_value = 0
        for g in self.genes:
            hash_value = hash_value + hash(g)
        return hash_value

    def __eq__(self, other):
        if len(self.genes) - len(other.genes) != 0:
            return False
        for g in self.genes:
            if g not in other.genes:
                return False
        for g in other.genes:
            if g not in self.genes:
                return False
        return True

class GeneClustersAccessions:
    def __init__(self):
        self.genes = dict() #key is an accession name, value is a set of GeneClusters


if not os.path.exists("pan_gene_dict"):
    homologousPairs = dict()
    pan_gene_dict = dict() # the key is the accession name, value is a list of GeneClusters
    allAccessions = set()
    gene_to_accession = dict()

    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for file in onlyfiles:
        accession_names = file.split('_')
        queryAccession = accession_names[0]
        referenceAccession = accession_names[1]
        allAccessions.add(queryAccession)
        allAccessions.add(referenceAccession)

        if referenceAccession not in pan_gene_dict:
            pan_gene_dict[referenceAccession] = set()
        f = open(mypath+file, 'r')
        firstline = f.readline().rstrip()
        for line in f:
            line = line.rstrip()
            queryGene = line.split()[0]
            refGenes = line.split()[1].split(";")
            geneClusters = GeneClusters()
            gene_to_accession[queryGene] = queryAccession
            if queryGene not in homologousPairs:
                homologousPairs[queryGene] = set()
            for gene in refGenes:
                gene_to_accession[gene] = referenceAccession
                homologousPairs[queryGene].add(gene)
                geneClusters.genes.add(gene)
                if gene not in homologousPairs:
                    homologousPairs[gene] = set()
                homologousPairs[gene].add(queryGene)
            pan_gene_dict[referenceAccession].add(geneClusters)
#            print(len(pan_gene_dict[referenceAccession]))
        f.close()

    print("line 96")
    with open("pan_gene_dict", 'wb') as f:
        pickle.dump(pan_gene_dict, f, protocol=4)

    with open("homologousPairs", 'wb') as f:
        pickle.dump(homologousPairs, f, protocol=4)

    with open("allAccessions", 'wb') as f:
        pickle.dump(allAccessions, f, protocol=4)

    with open("gene_to_accession", 'wb') as f:
        pickle.dump(gene_to_accession, f, protocol=4)
else:
    pan_gene_dict = pickle.load(open("pan_gene_dict", "rb"))
    homologousPairs = pickle.load(open("homologousPairs", "rb"))
    allAccessions = pickle.load(open("allAccessions", "rb"))
    gene_to_accession = pickle.load(open("gene_to_accession", "rb"))

for accession in pan_gene_dict:
    pan_gene_dict[accession] = list(pan_gene_dict[accession])

if not os.path.exists("pan_gene_dict2"):
    for accession in pan_gene_dict:
        print(accession + "\t" + str(len(pan_gene_dict[accession])))
        changed = True
        while changed:
            print("\t" + accession + "\t" + str(len(pan_gene_dict[accession])))
            changed = False
            pan_gene_dict_size = len(pan_gene_dict[accession])
            i = 0
            while i < (pan_gene_dict_size - 1):
                if 0 == i % 500:
                    print(i)
                j = i + 1
                while j < pan_gene_dict_size:
                    if len(pan_gene_dict[accession][i].genes.intersection(pan_gene_dict[accession][j].genes)) > 0:
                        pan_gene_dict[accession][i].genes = pan_gene_dict[accession][j].genes.union(pan_gene_dict[accession][i].genes)
                        del pan_gene_dict[accession][j]
                        pan_gene_dict_size = pan_gene_dict_size - 1
                        changed = True
                    else:
                        j = j + 1
                i = i + 1

    print("line 117")
    with open("pan_gene_dict2", 'wb') as f:
        pickle.dump(pan_gene_dict, f, protocol=4)
else:
    pan_gene_dict = pickle.load(open("pan_gene_dict2", "rb"))

accession_list = []
for a in allAccessions:
    accession_list.append(a)

pan_genes = pan_gene_dict[accession_list[0]]

i = 1
while i < len(accession_list):
    for this_pan_gene in pan_gene_dict[accession_list[i]]:
        ifMerged = False
        for pan_gene in pan_genes:
            for this_gene  in this_pan_gene:
                for gene in pan_gene:
                    if gene in homologousPairs[this_gene]:
                        ifMerged = True


#
# geneClustersAccessionsList = []
# def createGeneCluster(accession1, accession2, i, j):
#     for genei in pan_gene_dict[accession1][i].genes:
#         for genej in pan_gene_dict[accession2][j].genes:
#             if genei in homologousPairs and genej in homologousPairs[genei]:
#                 geneClustersAccessions = GeneClustersAccessions()
#                 geneClustersAccessions.genes[accession1] = {pan_gene_dict[accession1][i]}
#                 geneClustersAccessions.genes[accession2] = {pan_gene_dict[accession2][j]}
#                 geneClustersAccessionsList.append(geneClustersAccessions)
#                 return True
#     return False
#
#
# if not os.path.exists("geneClustersAccessionsList"):
#     i = 0
#     while i < len( accession_list):
#         j = i + 1
#         while j < len( accession_list):
#             for ii in range(len(pan_gene_dict[accession_list[i]])):
#                 print( accession_list[i] + "\t" + accession_list[j] + "\t" + str(ii) )
#                 for jj in range(len(pan_gene_dict[accession_list[j]])):
#                     createGeneCluster(accession_list[i], accession_list[j], ii, jj)
#             j = j + 1
#         i = i + 1
#     with open("geneClustersAccessionsList", 'wb') as f:
#         pickle.dump(geneClustersAccessionsList, f, protocol=4)
# else:
#     geneClustersAccessionsList = pickle.load(open("geneClustersAccessionsList", "rb"))
#
# print("line 140")
#
#
# def mergeGeneClustersAccessionsList(i, j):
#     for accession in geneClustersAccessionsList[i].genes:
#         if accession in geneClustersAccessionsList[j].genes:
#             if len(geneClustersAccessionsList[i].genes[accession].intersection( geneClustersAccessionsList[j].genes[accession] ) ) > 0:
#                     for accessionj in geneClustersAccessionsList[j].genes:
#                         if accessionj not in geneClustersAccessionsList[i].genes:
#                             geneClustersAccessionsList[i].genes[accessionj] = set()
#                         geneClustersAccessionsList[i].genes[accessionj] = geneClustersAccessionsList[i].genes[accessionj].union( geneClustersAccessionsList[j].genes[accessionj] )
#                     del geneClustersAccessionsList[j]
#                     return True
#     return False
#
# if not os.path.exists("geneClustersAccessionsList2"):
#     changed = True
#     while changed:
#         changed = False
#         geneClustersAccessionsList_size = len(geneClustersAccessionsList)
#         i = 0
#         while i < (geneClustersAccessionsList_size - 1):
#             j = i + 1
#             while j < geneClustersAccessionsList_size:
#                 if mergeGeneClustersAccessionsList(i, j):
#                     geneClustersAccessionsList_size = geneClustersAccessionsList_size - 1
#                     changed = True
#                 else:
#                     j = j + 1
#             i = i + 1
#     with open("geneClustersAccessionsList2", 'wb') as f:
#         pickle.dump(geneClustersAccessionsList, f, protocol=4)
# else:
#     geneClustersAccessionsList = pickle.load(open("geneClustersAccessionsList2", "rb"))
#
# for a in accession_list:
#     print(a, end = '\t')
# print()
#
# for geneClustersAccession in geneClustersAccessionsList:
#     for a in accession_list:
#         if a in geneClustersAccession.genes:
#             for geneCluster in geneClustersAccession.genes[a]:
#                 for g in geneCluster.genes:
#                     print(g, end = ';')
#                 print(end = ':')
#             print("", end = '\t')
#         else:
#             print("NA", end = '\t')
#     print()
