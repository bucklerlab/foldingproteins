from xgboost import XGBRegressor
from sklearn.model_selection import cross_val_score
import numpy as np
from sklearn.model_selection import train_test_split
from scipy import stats
from itertools import chain

protein_structure_profile_file = "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/BLINK/gd"
shaperMersvd1_expression = np.mat(np.genfromtxt(protein_structure_profile_file, skip_header=1, usecols= chain( range(11, 283882, 13)) , missing_values="NA", filling_values="nan")).astype("float")
col_mean = np.nanmean(shaperMersvd1_expression, axis=0)
inds = np.where(np.isnan(shaperMersvd1_expression))
shaperMersvd1_expression[inds] = np.take(col_mean, inds[1])



genes_dict = dict()
accession_dict = dict()
f1 = open(protein_structure_profile_file, 'r')
i = 0
for line in f1:
    line = line.strip()
    l = line.split()
    if i == 0:
        ii = -1
        for l1 in l:
            if ii >= 0:
                genes_dict[l1] = ii
            ii = ii + 1
    elif i > 0:
        accession_dict[l[0]] = i - 1
    i = i+1
f1.close()

phenotypeFile = "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/prepareData/all_NAM_phenos.csv"
phenotype = dict()
f1 = open(phenotypeFile, 'r')
i = 0
phenotype_tested = ""
#phenotypeColumn = 39
phenotypeColumn = 8
for line in f1:
    line = line.strip()
    l = line.split(",")
    if i > 0:
        if (l[phenotypeColumn] != "NA" and len(l[phenotypeColumn]) > 0):
            phenotype[l[0].strip('"')] = float(l[phenotypeColumn])
    else:
        phenotype_tested = l[phenotypeColumn]
    i = i + 1


y11 = []
x11 = []
for accession in phenotype:
    if accession in accession_dict:
        x11.append(shaperMersvd1_expression[:, accession_dict[accession]].flatten().tolist()[0])
        y11.append( phenotype[accession])

y11 = np.array(y11)
x11 = np.array(x11)

X_train, X_test, y_train, y_test = train_test_split(x11, y11, test_size=0.33, random_state=7)
model = XGBRegressor()
eval_set = [(X_train, y_train), (X_test, y_test)]
model.fit(X_train, y_train, eval_metric=["error", "logloss"], eval_set=eval_set, verbose=False)
y_pred = model.predict(X_test)

slope, intercept, r_value, p_value, std_err = stats.linregress(y_test, y_pred)
print(str(r_value))

