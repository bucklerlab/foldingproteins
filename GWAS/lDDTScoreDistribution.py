import os.path
import numpy as np
from numpy import linalg
from scipy import stats
import warnings
import re
import sys
import scipy.stats as ss
import os.path
from os import path

def parseGff( gffFile ):
    b73_chr = dict()
    b73_position = dict()
    f0 = open(gffFile, 'r')
    for line in f0:
        m = re.search('^(\S+)\s+(\S+)\s+gene\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        m2 = re.search('^(\S+)\s+(\S+)\s+mRNA\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        if( m != None ):
            b73_chr[m.group(5)]=m.group(1)
            b73_position[m.group(5)]=m.group(3)
        elif (  m2 != None ):
            b73_chr[m2.group(5)]=m2.group(1)
            b73_position[m2.group(5)]=m2.group(3)
    return b73_chr, b73_position

b73_chr, b73_position = parseGff("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3")

pagene_id_to_B73 = dict()
f0 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv", 'r')
for line in f0:
    l = list(map(str.strip, line.split(",")))
    gene = l[3]
    gene = gene.split(";")[0][:len(gene) - 5]
    pagene_id_to_B73[re.sub("pan_gene_", "", l[0])] = gene
myPath = "./"
for folder in os.listdir(myPath):
    if path.exists(myPath + folder + "/plDDTSummary"):
        f = open(myPath + folder + "/plDDTSummary", 'r')
        for line in f:
            line = line.strip()
            print(b73_chr[pagene_id_to_B73[folder]] + "\t" + b73_position[pagene_id_to_B73[folder]] + "\t" + line)

        if path.exists(myPath + folder + "/plDDTSummary"):
            f = open(myPath + folder + "/plDDTSummary", 'r')
            for line in f:
                line = line.strip()
                if len(line)>0:
                    print("\t" + line)
                else:

        else:
            print("\t")