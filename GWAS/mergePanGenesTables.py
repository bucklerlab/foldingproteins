import os
from os import listdir
from os.path import isfile, join
import pickle
import glob
import numpy as np

def readPanGene(panGeneFile):
    panGenesId_dict = dict()
    accession_dict = dict()
    f1 = open(panGeneFile, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split(",")
        if i == 0:
            ii = -3
            for l1 in l:
                if ii >= 0 and ii<=25:
                    accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            pan_gene = l[0]
            panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()

    panGenes = np.array(np.loadtxt(panGeneFile, skiprows=1, usecols = range(3, len(accession_dict)+3, 1), delimiter=',', dtype='str'))
    return panGenesId_dict, accession_dict, panGenes

panGenesId_dict, accession_dict, panGenes = readPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv")

pan_genes = dict()
accessions = []

for accession in accession_dict:
    accessions.append(accession)

for panGenesId in panGenesId_dict:
    if panGenesId not in pan_genes:
        pan_genes[panGenesId] = dict()
    for accession in accession_dict:
        for gene in panGenes[panGenesId_dict[panGenesId], accession_dict[accession]].split(";"):
            if gene.startswith("Zm"):
                if accession in pan_genes[panGenesId]:
                    pan_genes[panGenesId][accession] = pan_genes[panGenesId][accession] + ";" + gene
                else:
                    pan_genes[panGenesId][accession] = gene


tableFiles = (glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_genes_tables/pan_genes_tables/Z*"))
for tableFile in tableFiles:
    acession = os.path.basename(tableFile)
    accessions.append(acession)
    with open(tableFile) as f:
        for line in f:
            l = line.split()
            if l[0] != 'pan_gene_id' and l[1] != 'NA':
                if l[0] not in pan_genes:
                    pan_genes[l[0]] = dict()
                pan_genes[l[0]][acession] = l[1]
    f.close()

output = open("mergedPanGeneTables", 'w')
output.write( "pan_gene_id\t" + "\t".join(accessions) + "\n")
for pan_gene_id in pan_genes:
    output.write( pan_gene_id )
    for accession in accessions:
        if accession in pan_genes[pan_gene_id]:
            output.write( "\t" + pan_genes[pan_gene_id][accession] )
        else:
            output.write( "\tNA")
    output.write("\n")
output.close()
