from pathlib import Path
from ftplib import FTP
import prody as pd
from dataclasses import dataclass
import numpy as np
import typing as ty
from geometricus import MomentInvariants, SplitType
import tarfile
from time import time
from tqdm import tqdm
from scipy import ndimage
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
import itertools
from pathlib import Path
from sklearn.decomposition import NMF
import openTSNE
import os
import pickle
import re
import sys
from copy import deepcopy
import glob
# from src import uniprot_parser, proteinnet_parser
from sklearn.decomposition import TruncatedSVD
from os.path import exists
import csv
from statistics import mean
import scipy.stats as ss

def get_AF_shapemers(inputFiles,
                     resolution_kmer=4,
                     resolution_radius=6,
                     length_threshold=50):
    '''
    this function is modified from https://github.com/TurtleTools/alphafold-structural-space/blob/main/src/make_data.py
    '''

    unique_shape_mers = dict()
    shape_mers = []
    for accession, pdb_files in inputFiles.items():
        pdb_files_str = ';'.join(pdb_files)
        if pdb_files_str in unique_shape_mers:
            shape_mers.append(accession + "\t" + unique_shape_mers[pdb_files_str])
            continue
        index = 0
        shapemers = []
        for pdb_file in pdb_files:
#            print(accession + "\t" + pdb_file)
            pdb_file = Path(pdb_file)
            key = pdb_file.stem
            pdb = pd.parsePDB(str(pdb_file)).select("protein and calpha")
            betas = ndimage.gaussian_filter1d(pdb.getBetas(), sigma=5)
            coords = pdb.getCoords()
            sequence = pdb.getSequence()

            indices = np.ones(betas.shape[0], dtype=int)
            indices[np.where(betas < 70)] = 0

            slices = ndimage.find_objects(ndimage.label(indices)[0])
            for s in slices:
                s = s[0]
                if s.stop - s.start > length_threshold:
                    index += 1
                    invariants = MomentInvariants.from_coordinates(
                        key,
                        coords[s.start: s.stop],
                        sequence[s.start: s.stop],
                        split_type=SplitType.KMER_CUT,
                        split_size=16
                    )
                    shapemers += [f"k{x[0]}i{x[1]}i{x[2]}i{x[3]}" for x in (np.log1p(invariants.moments) * resolution_kmer).astype(int)]
                    invariants = MomentInvariants.from_coordinates(
                        key,
                        coords[s.start: s.stop],
                        sequence[s.start: s.stop],
                        split_type=SplitType.RADIUS,
                        split_size=10
                    )
                    shapemers += [f"r{x[0]}i{x[1]}i{x[2]}i{x[3]}" for x in
                                  (np.log1p(invariants.moments) * resolution_radius).astype(int)]
#        print(accession + "\tline_78\t" + str(index))
        if index > 0:
            #if len(pdb_files)>0:
            shape_mers.append( accession + "\t" + " ".join(shapemers) + "\n")
            unique_shape_mers[pdb_files_str] = (" ".join(shapemers) + "\n")
    return shape_mers

def readEnergyFiles(inputFiles):
    energies = {}
    for accession, energy_files in inputFiles.items():
        energies[accession] = []
        for energy_file in energy_files:
            with open(energy_file) as f:
                for line in f:
                    m = re.search('"final_energy": (\S+),', line)
                    if (m != None):
                        energies[accession].append( float(m.group(1)) )
    return energies

def readFastaFile(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[name] = s
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[name] = s
    return fastas


def readFastaFileRev(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[s] = name
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[s] = name
    return fastas


def readPanGene(panGeneFile):
    panGenesId_dict = dict()
    accession_dict = dict()
    f1 = open(panGeneFile, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split(",")
        if i == 0:
            ii = -3
            for l1 in l:
                if ii >= 0 and ii<=25:
                    accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            pan_gene = l[0]
            panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()

    panGenes = np.array(np.loadtxt(panGeneFile, skiprows=1, usecols = range(3, len(accession_dict)+3, 1), delimiter=',', dtype='str'))
    return panGenesId_dict, accession_dict, panGenes

maxInt = sys.maxsize
csv.field_size_limit(maxInt)
def readRilPanGene(rilPanGeneFile, pan_gene_id):
    ril_panGenesId_dict = dict()
    ril_accession_dict = dict()
    f1 = open(rilPanGeneFile, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split()
        if i == 0:
            ii = -1
            for l1 in l:
                if ii >= 0:
                    ril_accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            pan_gene = l[0]
            ril_panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()
    row_index = ril_panGenesId_dict[pan_gene_id] + 1
    with open(rilPanGeneFile, 'r') as fin:
        reader=csv.reader(fin)
        result=[[s for s in row] for i,row in enumerate(reader) if i == row_index]
    return ril_accession_dict, result[0][0].split()
#
# panGenesId_dict, accession_dict, panGenes = readPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv")
# fastas = readFastaFile( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/all.proteins.fas")
# fastasRev = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/unique.seqs.fa")
# fastasRev2 = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/unique.seqs.fa")
#
# with open("panGenesId_dict", 'wb') as f:
#     pickle.dump(panGenesId_dict, f, protocol=4)
#
# with open("accession_dict", 'wb') as f:
#     pickle.dump(accession_dict, f, protocol=4)
#
# with open("panGenes", 'wb') as f:
#     pickle.dump(panGenes, f, protocol=4)
#
# with open("fastas", 'wb') as f:
#     pickle.dump(fastas, f, protocol=4)
#
# with open("fastasRev", 'wb') as f:
#     pickle.dump(fastasRev, f, protocol=4)
#
# with open("fastasRev2", 'wb') as f:
#     pickle.dump(fastasRev2, f, protocol=4)

pan_group_id =sys.argv[1]
#if not exists("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/shapmer/shapmer/" + pan_group_id):
if not exists("/workdir/bs674/shapmer/" + pan_group_id):
    # panGenesId_dict = pickle.load(open("panGenesId_dict", "rb"))
    # accession_dict = pickle.load(open("accession_dict", "rb"))
    # panGenes = pickle.load(open("panGenes", "rb"))
    fastas = pickle.load(open("fastas", "rb"))
    fastasRev = pickle.load(open("fastasRev", "rb"))
    fastasRev2 = pickle.load(open("fastasRev2", "rb"))

    #ril_accession_dict, rilPanGene = readRilPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_genes_tables/mergedPanGeneTables", pan_group_id)
    # print(ril_accession_dict)
    # print(rilPanGene)
    ril_accession_dict, rilPanGene = readRilPanGene("/workdir/bs674/shapmer/mergedPanGeneTables", pan_group_id)

#    print(ril_accession_dict)
    pdbFiles = {}
    energyFiles = {}
    for accession in ril_accession_dict:
        # print("line 235" + accession)
        pdbFiles[accession] = []
        energyFiles[accession] = []
        geneIds = rilPanGene[ril_accession_dict[accession]+1].split(";")
        # print(geneIds)
        for geneid in geneIds:
            if geneid in fastas:
                if fastas[geneid] in fastasRev:
                    #if len(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.pdb")) >0:
                    if len(glob.glob("/workdir/bs674/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.pdb")) >0:
    #                    print("line 246 " + fastasRev[fastas[geneid]])
                        pdbFiles[accession].append(glob.glob("/workdir/bs674/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.pdb")[0])
                        #pdbFiles[accession].append(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.pdb")[0])
                        #energyFiles[accession].append(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.energy")[0])
                        energyFiles[accession].append(glob.glob("/workdir/bs674/predictedStructures/otherAccessions/" + fastasRev[fastas[geneid]] + "/*maximumplddts.energy")[0])
                    # else:
                    #     print(fastasRev[fastas[geneid]] + "\t missing")
                    # print("find in fastasRevH " + geneid + "\t" + fastasRev[fastas[geneid]])
                elif fastas[geneid] in fastasRev2:
                    #if len(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.pdb")) >0:
                    if len(glob.glob("/workdir/bs674/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.pdb")) >0:
     #                   print("line 252 " + fastasRev2[fastas[geneid]])
                        pdbFiles[accession].append(glob.glob("/workdir/bs674/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.pdb")[0])
                        #pdbFiles[accession].append(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.pdb")[0])
                        #energyFiles[accession].append(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.energy")[0])
                        energyFiles[accession].append(glob.glob("/workdir/bs674/predictedStructures/B73_result/" + fastasRev2[fastas[geneid]] + "/*maximumplddts.energy")[0])
                #     else:
                #         print(fastasRev2[fastas[geneid]] + "\t B73missing" )
                #     print("find in fastasRev2H " + geneid + "\t" + fastasRev2[fastas[geneid]])
                # else:
                #     print("could not find in fastasRev and fastasRev2 " + geneid)
            # else:
            #     print("could not find in fastas " + geneid)

    # if len(pdbFiles) < 26 or len(energyFiles) < 26:
    #     sys.exit()

    shape_mers = get_AF_shapemers(pdbFiles)
    energies = readEnergyFiles(energyFiles)
#    print(energies)

    keys_corpus = (line.strip().split("\t") for line in shape_mers)

    keys, corpus = itertools.tee(keys_corpus)

    keys = [k[0] for k in keys]
    corpus = (k[1] for k in corpus)

    # print(keys)
    # for c in corpus:
    #     print(c)
#    print(len(corpus))

    vectorizer = TfidfVectorizer(min_df=1)
    tfidf_matrix = vectorizer.fit_transform(corpus)
    svd = TruncatedSVD(n_components=3, random_state=42)
    X_svd = svd.fit_transform(tfidf_matrix)
    svd_compress = np.array(X_svd)

    # print(pan_group_id +"\t" + str(svd.explained_variance_ratio_.sum()))
    # print(svd.explained_variance_ratio_)
    # print(svd.explained_variance_ratio_[0:2])
    # print(svd.explained_variance_ratio_[0:2].sum())

    # i=0
    # print(keys[i] + "\t" + rilPanGene[ril_accession_dict[keys[i]]+1] + "\t" + str(svd_compress[i][0]) + "\t" + str(svd_compress[i][1]) +  "\t" + str(svd_compress[i][2]) +  "\t" + str(min(energies[keys[i]])) + "\t" + str(max(energies[keys[i]])) + "\t" + str(mean(energies[keys[i]])) + "\t" + ";".join(map(str, energies[keys[i]])) + "\n")
    # i=1
    # print(keys[i] + "\t" + rilPanGene[ril_accession_dict[keys[i]]+1] + "\t" + str(svd_compress[i][0]) + "\t" + str(svd_compress[i][1]) +  "\t" + str(svd_compress[i][2]) +  "\t" + str(min(energies[keys[i]])) + "\t" + str(max(energies[keys[i]])) + "\t" + str(mean(energies[keys[i]])) + "\t" + ";".join(map(str, energies[keys[i]])) + "\n")

    order = ss.rankdata(svd.explained_variance_ratio_).tolist()
    order = [3 - element for element in order]
    order_dict = dict()
    for index, value in enumerate(order):
        value = int(value)
        order_dict[value]=index

    # i=0
    # print(keys[i] + "\t" + rilPanGene[ril_accession_dict[keys[i]]+1] + "\t" + str(svd_compress[i][order_dict[0]]) + "\t" + str(svd_compress[i][order_dict[1]]) +  "\t" + str(svd_compress[i][order_dict[2]]) +  "\t" + str(min(energies[keys[i]])) + "\t" + str(max(energies[keys[i]])) + "\t" + str(mean(energies[keys[i]])) + "\t" + ";".join(map(str, energies[keys[i]])) + "\n")
    # i=1
    # print(keys[i] + "\t" + rilPanGene[ril_accession_dict[keys[i]]+1] + "\t" + str(svd_compress[i][order_dict[0]]) + "\t" + str(svd_compress[i][order_dict[1]]) +  "\t" + str(svd_compress[i][order_dict[2]]) +  "\t" + str(min(energies[keys[i]])) + "\t" + str(max(energies[keys[i]])) + "\t" + str(mean(energies[keys[i]])) + "\t" + ";".join(map(str, energies[keys[i]])) + "\n")


    #output = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/shapmer/shapmer/" + pan_group_id, 'w')
    output = open("/workdir/bs674/shapmer/" + pan_group_id, 'w')
    for i in range(len(keys)):
        output.write(keys[i] + "\t" + rilPanGene[ril_accession_dict[keys[i]]+1] + "\t" + str(svd_compress[i][order_dict[0]]) + "\t" + str(svd.explained_variance_ratio_[order_dict[0]]) + "\t" + str(svd_compress[i][order_dict[1]]) + "\t" + str(svd.explained_variance_ratio_[order_dict[1]]) +  "\t" + str(svd_compress[i][order_dict[2]]) + "\t" + str(svd.explained_variance_ratio_[order_dict[2]]) +  "\t" + str(min(energies[keys[i]])) + "\t" + str(max(energies[keys[i]])) + "\t" + str(mean(energies[keys[i]])) + "\t" + ";".join(map(str, energies[keys[i]])) + "\n")
    output.close()

