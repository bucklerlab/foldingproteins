from pathlib import Path
from ftplib import FTP
import prody as pd
from dataclasses import dataclass
import numpy as np
import typing as ty
from geometricus import MomentInvariants, SplitType
import tarfile
from time import time
from tqdm import tqdm
from scipy import ndimage
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
import itertools
from pathlib import Path
from sklearn.decomposition import NMF
import openTSNE
import os
import pickle
import re
import sys
from copy import deepcopy
import glob
# from src import uniprot_parser, proteinnet_parser
from sklearn.decomposition import TruncatedSVD
from os.path import exists
import csv
from statistics import mean
import scipy.stats as ss

def get_AF_shapemers(pdb_file,
                     resolution_kmer=4,
                     resolution_radius=6,
                     length_threshold=50):
    '''
    this function is modified from https://github.com/TurtleTools/alphafold-structural-space/blob/main/src/make_data.py
    '''

    pdb_file = Path(pdb_file)
    key = pdb_file.stem
    pdb = pd.parsePDB(str(pdb_file)).select("protein and calpha")
    betas = ndimage.gaussian_filter1d(pdb.getBetas(), sigma=5)
    coords = pdb.getCoords()
    sequence = pdb.getSequence()

    indices = np.ones(betas.shape[0], dtype=int)
    indices[np.where(betas < 70)] = 0

    slices = ndimage.find_objects(ndimage.label(indices)[0])
    for s in slices:
        s = s[0]
        if s.stop - s.start > length_threshold:
            print(sequence[s.start: s.stop])
            invariants = MomentInvariants.from_coordinates(
                key,
                coords[s.start: s.stop],
                sequence[s.start: s.stop],
                split_type=SplitType.KMER_CUT,
                split_size=16
            )
#            shapemers += [f"k{x[0]}i{x[1]}i{x[2]}i{x[3]}" for x in (np.log1p(invariants.moments) * resolution_kmer).astype(int)]
            for x in (np.log1p(invariants.moments) * resolution_kmer).astype(int):
                print(f"k{x[0]}i{x[1]}i{x[2]}i{x[3]}")

            invariants = MomentInvariants.from_coordinates(
                key,
                coords[s.start: s.stop],
                sequence[s.start: s.stop],
                split_type=SplitType.RADIUS,
                split_size=10
            )
            for x in (np.log1p(invariants.moments) * resolution_radius).astype(int):
                print(f"r{x[0]}i{x[1]}i{x[2]}i{x[3]}")
#

pdb_file = sys.argv[1]
get_AF_shapemers(pdb_file)

