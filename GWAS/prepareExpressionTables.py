"""
Created: 25/12/2017  00:40:39
Author: Baoxing Song
Email: songbaoxing168@163.com
This source code is partially adopted from https://github.com/bvilhjal/mixmogam/
And the R source code of emma is referred

modified: 31 Oct 2019
By Baoxing Song try to take the CNS present/absent as genotypic variant for GWAS analysis using a fixed model
about the f test implemented here https://towardsdatascience.com/fisher-test-for-regression-analysis-1e1687867259
"""

import numpy as np
# get the PCS taxa id to line number map. line number could be queried from the genotypic variants


def readImputatedGeneExpression(file, number_of_column):
    #expression_table = np.mat(np.loadtxt(file, skiprows=1, delimiter=',', usecols = range(number_of_column)), filling_values=99).astype("float") # this is the pcs matrix
    expression_table = np.mat(np.genfromtxt(file, skip_header=1, delimiter=',', usecols = range(number_of_column),  filling_values=0)).astype("float") # this is the pcs matrix
    accession_dict = dict()
    gene_dict = dict()

    f1 = open(file, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split(",")
        if i == 0:
            ii = 0
            for l1 in l:
                gene_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            accession = l[ len(l)-1 ]
            accession_dict[accession] = i - 1
        i = i+1
    f1.close()
    return expression_table, accession_dict, gene_dict

shoot_expression_table, shoot_accession_dict, shoot_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_GShoot_cis_trans_model.csv", number_of_column=18093)
L3Tip_expression_table, L3Tip_accession_dict, L3Tip_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_L3Tip_cis_trans_model.csv", number_of_column=17921)
LMAD_expression_table, LMAD_accession_dict, LMAD_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_LMAD_cis_trans_model.csv", number_of_column=17968)
L3Base_expression_table, L3Base_accession_dict, L3Base_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_L3Base_cis_trans_model.csv", number_of_column=18047)
Kern_expression_table, Kern_accession_dict, Kern_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_Kern_cis_trans_model.csv", number_of_column=17940)
root_expression_table, root_accession_dict, root_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_GRoot_cis_trans_model.csv", number_of_column=18090)
LMAN_expression_table, LMAN_accession_dict, LMAN_gene_dict = readImputatedGeneExpression("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/exp_NAM_LMAN_cis_trans_model.csv", number_of_column=17963)

v3_to_v5_dict = dict()
f1 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/B73v3_B73v5_liftover_genemodel_CDS_xref_shortened.txt", 'r')
for line in f1:
    line = line.strip()
    l = line.split()
    v3gene = l[0]
    v5gene = l[1]
    if v5gene.startswith("Zm"):
        v3_to_v5_dict[v3gene] = v5gene

for gene in shoot_gene_dict:
    if (gene in v3_to_v5_dict) and (gene in L3Tip_gene_dict) and (gene in LMAD_gene_dict) and (gene in L3Base_gene_dict) and (gene in Kern_gene_dict) and (gene in root_gene_dict) and (gene in LMAN_gene_dict):
        output = open("./expressionforeachGene/" + v3_to_v5_dict[gene], 'w')
        for accession in shoot_accession_dict:
            output.write( accession + "\t" + str(shoot_expression_table[shoot_accession_dict[accession], shoot_gene_dict[gene]]) + "\t" +
                          str(L3Tip_expression_table[L3Tip_accession_dict[accession], L3Tip_gene_dict[gene]]) + "\t" +
                          str(LMAD_expression_table[LMAD_accession_dict[accession], LMAD_gene_dict[gene]]) + "\t" +
                          str(L3Base_expression_table[L3Base_accession_dict[accession], L3Base_gene_dict[gene]]) + "\t" +
                          str(Kern_expression_table[Kern_accession_dict[accession], Kern_gene_dict[gene]]) + "\t" +
                          str(root_expression_table[root_accession_dict[accession], root_gene_dict[gene]]) + "\t" +
                          str(LMAN_expression_table[LMAN_accession_dict[accession], LMAN_gene_dict[gene]]) + "\n"
                         )
        output.close()
