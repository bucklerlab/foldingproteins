import re
import sys
import os
from os import listdir
from os.path import isfile, join

def parseGff( gffFile ):
    b73_chr = dict()
    b73_position = dict()
    f0 = open(gffFile, 'r')
    for line in f0:
        m = re.search('^(\S+)\s+(\S+)\s+gene\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        m2 = re.search('^(\S+)\s+(\S+)\s+mRNA\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        if( m != None ):
            b73_chr[m.group(5)]=m.group(1)
            b73_position[m.group(5)]=m.group(3)
        elif (  m2 != None ):
            b73_chr[m2.group(5)]=m2.group(1)
            b73_position[m2.group(5)]=m2.group(3)
    return b73_chr, b73_position

#def parse_protein_structure(folder="../shapmer/shapmer/"):
def parse_protein_structure(folder="../shapmer/"):
    energy = {}
    shapermerSvd = {}
    all_NAMids = set()
    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    for (file) in onlyfiles:
        if file.startswith("pan_gene_"):
            energy[file] = dict() # first key is pangene_id
            shapermerSvd[file] = dict()
            with open(folder+file) as f:
                for line_i, line in enumerate(f):
                    l = line.split()
                    all_NAMids.add(l[0])
                    energy[file][l[0]] = [float(l[8]), float(l[9]), float(l[10])]
                    shapermerSvd[file][l[0]] = [float(l[2]), float(l[3]), float(l[4]),float(l[5]), float(l[6]), float(l[7])]
    return energy, shapermerSvd, all_NAMids

#f = open("/media/bs674/ppi8t/NAMpopulation_alphafold/phenotypes/0_curated_phenotypes/nam/complete_nxp/all_NAM_phenos.csv", 'r')
f = open("./all_NAM_phenos.csv", 'r')
line_index = 0
phen_index = {}  #key is the phenotype id, value is the column index
phen_individs = {} # key is the taxa name and value is the phenotype value

for line in f:
    line = line.strip()
    if 0 == line_index:
        l = line.split(",")
        for i in range(0, len(l)):
            stripped_li = l[i].strip('"')
            phen_index[stripped_li] = i
    else:
        l = line.split(",")
        phen_individs[l[0].strip('"')] = l #float(l[phen_index[phenotype]])
    line_index = line_index + 1

f.close()
#read phenotype data end

#energy, shapermerSvd, all_NAMids = parse_protein_structure("../shapmer/shapmer/")
energy, shapermerSvd, all_NAMids = parse_protein_structure("../shapmer/")
#print(energy)
print("energy reading done")
#b73_chr, b73_position = parseGff("/media/bs674/ppi8t/NAM_anchorwave/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3")
b73_chr, b73_position = parseGff("./Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3")
print("gff3 reading done")
pagene_id_to_B73 = dict()
#f0 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv", 'r')
f0 = open("./pan_gene_matrix_v3_cyverse.csv", 'r')
for line in f0:
    l = list(map(str.strip, line.split(",")))
    gene = l[3]
    gene = gene[:len(gene) - 5]
    pagene_id_to_B73[l[0]] = gene

print("panGene reading done")

expression_dict = dict()
for pan_gene in energy:
    if len(pan_gene)>0 and (pan_gene in pagene_id_to_B73) and (len(pagene_id_to_B73[pan_gene])>0):
        expression_dict[pan_gene] = dict()
        #if os.path.exists("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[pan_gene]):
        if os.path.exists("./expressionforeachGene/" + pagene_id_to_B73[pan_gene]):
            #f = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[pan_gene], 'r')
            f = open("./expressionforeachGene/" + pagene_id_to_B73[pan_gene], 'r')
            for line in f:
                l = line.split()
                expression_dict[pan_gene][l[0]] = [float(l[1]), float(l[2]), float(l[3]), float(l[4]), float(l[5]), float(l[6]), float(l[6])]

print("gene_expression reading done")

pan_genes = []
for pan_gene in energy:
    pan_genes.append(pan_gene)

gd_output = open("gd", 'w')
gd_output.write("Taxa")
for pan_gene in pan_genes:
    gd_output.write("\t" + pan_gene+"_shoot" + "\t" + pan_gene+"_L3Tip" + "\t" + pan_gene+"_LMAD" + "\t" + pan_gene+"_L3Base" + "\t" + pan_gene+"_Kern" + "\t" + pan_gene+"_root" + "\t" + pan_gene+"_LMAN" + "\t" + pan_gene+"_minFoldingEnergy" + "\t" + pan_gene+"_maxFoldingEnergy" + "\t" + pan_gene+"_meanFoldingEnergy" + "\t" + pan_gene+"_shapemerSvd1" + "\t" + pan_gene+"_shapemerSvd1_v" + "\t" + pan_gene+"_shapemerSvd2" +"\t" + pan_gene+"_shapemerSvd2_v" +  "\t" + pan_gene+"_shapemerSvd3" + "\t" + pan_gene+"_shapemerSvd3_v" )
gd_output.write("\n")

individuals = []

for individual in phen_individs:
    if individual in all_NAMids:
        individuals.append(individual)
        gd_output.write(individual)
        for pan_gene in pan_genes:
            if (pan_gene in expression_dict) and (individual in expression_dict[pan_gene]):
                gd_output.write("\t" + str(expression_dict[pan_gene][individual][0]) + "\t" + str(expression_dict[pan_gene][individual][1]) + "\t" + str(expression_dict[pan_gene][individual][2]) + "\t" + str(expression_dict[pan_gene][individual][3]) + "\t" + str(expression_dict[pan_gene][individual][4]) + "\t" + str(expression_dict[pan_gene][individual][5]) + "\t" + str(expression_dict[pan_gene][individual][6]))
            else:
                gd_output.write("\tNA\tNA\tNA\tNA\tNA\tNA\tNA")
                #gd_output.write("\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 )

            if (pan_gene in energy) and (individual in energy[pan_gene]):
                gd_output.write("\t" + str(energy[pan_gene][individual][0]) + "\t" + str(energy[pan_gene][individual][1]) + "\t" + str(energy[pan_gene][individual][2]) + "\t" + str(shapermerSvd[pan_gene][individual][0]) + "\t" + str(shapermerSvd[pan_gene][individual][1])+ "\t" + str(shapermerSvd[pan_gene][individual][2]) + "\t" + str(shapermerSvd[pan_gene][individual][3]) + "\t" + str(shapermerSvd[pan_gene][individual][4])+ "\t" + str(shapermerSvd[pan_gene][individual][5]))
            else:
                gd_output.write("\tNA\tNA\tNA\tNA\tNA\tNA")
        gd_output.write("\n")
gd_output.close()

#output phenotype data begin
for phen in phen_index:
    phenotype_output = open(phen, 'w')
    phenotype_output.write("Taxa" + "\t" + phen + "\n")
    for individual in individuals:
        phenotype_output.write(individual + "\t" + phen_individs[individual][phen_index[phen]] + "\n") #here l is not right
    phenotype_output.close()

cv_output = open("cv", 'w')
cv_output.write("Taxa\tQ1\tQ2\tQ3\tQ4\tQ5\tQ6\tQ7\tQ8\tQ9\tQ10\tQ11\tQ12\tQ13\tQ14\tQ15\tQ16\tQ17\tQ18\tQ19\tQ20\tQ21\tQ22\tQ23\tQ24\n")
for individual in individuals:
    if individual.startswith( 'Z001'):
        cv_output.write( individual + "\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z002'):
        cv_output.write( individual + "\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z003'):
        cv_output.write( individual + "\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z004'):
        cv_output.write( individual + "\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z005'):
        cv_output.write( individual + "\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z006'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z007'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z008'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z009'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z010'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z011'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z012'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z013'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z014'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z015'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z016'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z018'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z019'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z020'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z021'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z022'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\n")
    elif individual.startswith( 'Z023'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\n")
    elif individual.startswith( 'Z024'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\n")
    elif individual.startswith( 'Z025'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\n")
    elif individual.startswith( 'Z026'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    else:
        print("end of family, here is something wrong:" + individual)

cv_output.close()

gm_output = open("gm", 'w')
gm_output.write("Name\tChromosome\tPosition\n")

for pan_gene in pan_genes:
    if pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].startswith("Zm"):
        gm_output.write(pan_gene+"_shoot" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\n")
        gm_output.write(pan_gene+"_L3Tip" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_LMAD" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_L3Base" "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_Kern" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_root" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write( pan_gene+"_LMAN" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_minFoldingEnergy" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_maxFoldingEnergy" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_meanFoldingEnergy" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_shapemerSvd1" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_shapemerSvd2" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
        gm_output.write(pan_gene+"_shapemerSvd3" + "\t" + b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + str(int(b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]])+0) + "\n")
    elif pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].startswith("gmap"):
        gm_output.write(pan_gene+"_shoot\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_L3Tip\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_LMAD\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_L3Base\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_Kern\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_root\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write( pan_gene+"_LMAN\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_minFoldingEnergy\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_maxFoldingEnergy\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_meanFoldingEnergy\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_shapemerSvd1\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_shapemerSvd2\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
        gm_output.write(pan_gene+"_shapemerSvd3\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
    else:
        print (pan_gene)
        print (pagene_id_to_B73[pan_gene.split(";")[0]])
        print (pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0])
gm_output.close()
