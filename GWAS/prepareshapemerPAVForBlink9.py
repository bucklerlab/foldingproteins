import re
import sys
import os
from os import listdir
from os.path import isfile, join

def parseGff( gffFile ):
    b73_chr = dict()
    b73_position = dict()
    f0 = open(gffFile, 'r')
    for line in f0:
        m = re.search('^(\S+)\s+(\S+)\s+gene\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        m2 = re.search('^(\S+)\s+(\S+)\s+mRNA\s+(\S+)\s+(\S+)\s+.*ID=(\w+);', line)
        if( m != None ):
            b73_chr[m.group(5)]=m.group(1)
            b73_position[m.group(5)]=m.group(3)
        elif (  m2 != None ):
            b73_chr[m2.group(5)]=m2.group(1)
            b73_position[m2.group(5)]=m2.group(3)
    return b73_chr, b73_position

#def parse_protein_structure(folder="../shapmer/shapmer/"):
def parse_protein_structure(folder="../shapmer/"):
    shapermerValues = {}
    all_NAMids = set()
    shape_mers = {}
    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    for (file) in onlyfiles:
        #if file.startswith("pan_gene_9"):
        if file == "pan_gene_100":
            shapermerValues[file] = dict()
            with open(folder+file) as f:
                for line_i, line in enumerate(f):
                    l = line.split()
                    if line_i == 0:
                        shape_mers[file] = l[2:]
                    else:
                        all_NAMids.add(l[0])
                        shapermerValues[file][l[0]] = l[2:]
    return shapermerValues, all_NAMids, shape_mers

#f = open("/media/bs674/ppi8t/NAMpopulation_alphafold/phenotypes/0_curated_phenotypes/nam/complete_nxp/all_NAM_phenos.csv", 'r')
f = open("./all_NAM_phenos.csv", 'r')
line_index = 0
phen_index = {}  #key is the phenotype id, value is the column index
phen_individs = {} # key is the taxa name and value is the phenotype value

for line in f:
    line = line.strip()
    if 0 == line_index:
        l = line.split(",")
        for i in range(0, len(l)):
            stripped_li = l[i].strip('"')
            phen_index[stripped_li] = i
    else:
        l = line.split(",")
        phen_individs[l[0].strip('"')] = l #float(l[phen_index[phenotype]])
    line_index = line_index + 1

f.close()
#read phenotype data end

shapermerValues, all_NAMids, shape_mers = parse_protein_structure("../shapmerpav/")
b73_chr, b73_position = parseGff("./Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3")
print("gff3 reading done")
pagene_id_to_B73 = dict()

#f0 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv", 'r')
f0 = open("./pan_gene_matrix_v3_cyverse.csv", 'r')
for line in f0:
    l = list(map(str.strip, line.split(",")))
    gene = l[3]
    gene = gene[:len(gene) - 5]
    pagene_id_to_B73[l[0]] = gene

print("panGene reading done")


pan_genes = []
for pan_gene in shapermerValues:
    pan_genes.append(pan_gene)

gd_output = open("gdPAV9.ped", 'w')
# gd_output.write("Taxa")
# for pan_gene in pan_genes:
#     for shape_mer in shape_mers:
#         gd_output.write("\t" + pan_gene + "_" + shape_mer )
# gd_output.write("\n")

individuals = []

for individual in phen_individs:
    if individual in all_NAMids:
        individuals.append(individual)
        gd_output.write(individual + " " + individual+" 0 0 1\t1")
        for pan_gene in pan_genes:
            for value in shapermerValues[pan_gene][individual]:
                if value == "0":
                    gd_output.write("\t1 1")
                elif value == "2":
                    gd_output.write("\t2 2")
                else:
                    gd_output.write("\t-9 -9")
        gd_output.write("\n")
gd_output.close()

#output phenotype data begin
for phen in phen_index:
    phenotype_output = open(phen, 'w')
    phenotype_output.write("Taxa" + "\t" + phen + "\n")
    for individual in individuals:
        phenotype_output.write(individual + "\t" + phen_individs[individual][phen_index[phen]] + "\n") #here l is not right
    phenotype_output.close()

cv_output = open("cvPAV", 'w')
cv_output.write("Taxa\tQ1\tQ2\tQ3\tQ4\tQ5\tQ6\tQ7\tQ8\tQ9\tQ10\tQ11\tQ12\tQ13\tQ14\tQ15\tQ16\tQ17\tQ18\tQ19\tQ20\tQ21\tQ22\tQ23\tQ24\n")
for individual in individuals:
    if individual.startswith( 'Z001'):
        cv_output.write( individual + "\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z002'):
        cv_output.write( individual + "\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z003'):
        cv_output.write( individual + "\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z004'):
        cv_output.write( individual + "\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z005'):
        cv_output.write( individual + "\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z006'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z007'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z008'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z009'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z010'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z011'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z012'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z013'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z014'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z015'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z016'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z018'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z019'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z020'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z021'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\t0\n")
    elif individual.startswith( 'Z022'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\t0\n")
    elif individual.startswith( 'Z023'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\t0\n")
    elif individual.startswith( 'Z024'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\t0\n")
    elif individual.startswith( 'Z025'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t1\n")
    elif individual.startswith( 'Z026'):
        cv_output.write( individual + "\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n")
    else:
        print("end of family, here is something wrong:" + individual)

cv_output.close()

gm_output = open("gdPAV100.map", 'w')

for pan_gene in pan_genes:
    if pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].startswith("Zm"):
        for shape_mer in shape_mers[pan_gene]:
            gm_output.write(b73_chr[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\t" + pan_gene + "_" + shape_mer + "\t0\t" + b73_position[pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0]] + "\n")
    elif pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].startswith("gmap"):
        for shape_mer in shape_mers[pan_gene]:
            gm_output.write(pagene_id_to_B73[pan_gene.split(";")[0]].split(":")[0].split("=")[1].split(":")[0] + "\t" + pan_gene + "_" + shape_mer + "\t0\t" + "\t" + pagene_id_to_B73[pan_gene.split(";")[0]].split(";")[0].split(":")[1].split("-")[0] + "\n")
    else:
        for shape_mer in shape_mers[pan_gene]:
            gm_output.write("0\t" + pan_gene + "_" + shape_mer + "\t0\t0\n")


gm_output.close()
