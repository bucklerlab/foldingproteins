import re
import sys

def readFastaFile(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[name] = s
                name = m.group(1)
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[name] = s
    return fastas


def readFastaFileRev(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[s] = name
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[s] = name
    return fastas

fastas = readFastaFile( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/all.proteins.fas")
fastasRev = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/unique.seqs.fa")
fastasRev2 = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/unique.seqs.fa")

geneid = sys.argv[1]

if fastas[geneid] in fastasRev:
    print("other " + fastasRev[fastas[geneid]])
elif fastas[geneid] in fastasRev2:
    print("B73 " + fastasRev2[fastas[geneid]])
