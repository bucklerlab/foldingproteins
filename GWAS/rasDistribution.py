# Copyright 2021 DeepMind Technologies Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""lDDT protein distance score."""
import numpy as np
import re
import sys
import MyUtil

# bs674@cornell.edu

# this function was copied from the alphafold source code


def readMsa(fastaFile, uniqueSeq = True):
    if uniqueSeq:
        seqToName = {}
        name = ""
        seq = []
        with open(fastaFile) as f:
            for line in f:
                m = re.search('^>(\S+)', line)
                if (m != None) and line[:1] != "#":
                    if (len(name) > 0) & (len(seq) > 0):
                        s = ''.join(seq)
                        s = re.sub("\\s", "", s)
                        s = s.upper()
                        if s in seqToName:
                            seqToName[s] = seqToName[s] + "," + name
                        else:
                            seqToName[s] = name
                    name = m.group(1)
                    name = re.sub(":A", "", name)
                    seq = []
                elif line[:1] != "#":
                    seq.append(line)
            if (len(name) > 0) & (len(seq) > 0):
                s = ''.join(seq)
                s = re.sub("\\s", "", s)
                s = s.upper()
                if s in seqToName:
                    seqToName[s] = seqToName[s] + "," + name
                else:
                    seqToName[s] = name
        seq_names = []
        fastas = {}
        for seq in sorted(seqToName):
            seq_names.append(seqToName[seq])
            fastas[seqToName[seq]]=seq
        return seq_names, fastas
    else:
        fastas = {}
        seq_names = []
        name = ""
        seq = []
        with open(fastaFile) as f:
            for line in f:
                m = re.search('^>(\S+)', line)
                if (m != None) and line[:1] != "#":
                    if (len(name) > 0) and (len(seq) > 0):
                        s = ''.join(seq)
                        s = re.sub("\\s", "", s)
                        s = s.upper()
                        fastas[name] = s
                        seq_names.append(name)
                    name = m.group(1)
                    name = re.sub(":A", "", name)
                    seq = []
                elif line[:1] != "#":
                    seq.append(line)
            if (len(name) > 0) & (len(seq) > 0):
                s = ''.join(seq)
                s = re.sub("\\s", "", s)
                s = s.upper()
                fastas[name] = s
                seq_names.append(name)
        return seq_names, fastas

def readPdb(pdbFile):
    rsa = []
    with open(pdbFile) as f:
        for line in f:
            elements = line.split()
            if len(elements)>10 and elements[0] == elements[1] and elements[2] == "A":
                sa = float(line[35:38])
                rsa.append(sa/MyUtil.residue_max_acc[elements[3]])
    return np.array(rsa)

if __name__ == '__main__':
    pan_group_id = sys.argv[1]
    seq_names, msaFastas = readMsa("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations2/" + pan_group_id + "/sequences.mafft", uniqueSeq = False)
    rsas = []
    for i, seq_name_i in enumerate(seq_names):
        pdbFileRef = "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations2/" + pan_group_id + "/" + seq_name_i + ".mkdssp"
        rsas.append( np.mean(readPdb(pdbFileRef)))
    rsas = np.array(rsas)
    output = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations2/" + pan_group_id + "/rasSummary", 'w')
    output.write( str(np.mean(rsas)) + "\t" + str(np.median(rsas)) + "\t" + str(np.max(rsas)) + "\t" + str(np.std(rsas)) + "\t" + str(np.min(rsas)) + "\n" )
    output.close()
