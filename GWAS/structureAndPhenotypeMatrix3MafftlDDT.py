from pathlib import Path
from ftplib import FTP
import prody as pd
from dataclasses import dataclass
import numpy as np
import typing as ty
from geometricus import MomentInvariants, SplitType
import tarfile
from time import time
from tqdm import tqdm
from scipy import ndimage
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
import itertools
from pathlib import Path
from sklearn.decomposition import NMF
import openTSNE
import os
import pickle
import re
import sys
from copy import deepcopy
import glob
# from src import uniprot_parser, proteinnet_parser
from sklearn.decomposition import TruncatedSVD
from os.path import exists
import csv
from statistics import mean
import scipy.stats as ss

def readFastaFile(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[name] = s
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[name] = s
    return fastas


def readFastaFileRev(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[s] = name
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[s] = name
    return fastas


def readPanGene(panGeneFile):
    panGenesId_dict = dict()
    accession_dict = dict()
    f1 = open(panGeneFile, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split(",")
        if i == 0:
            ii = -3
            for l1 in l:
                if ii >= 0 and ii<=25:
                    accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            pan_gene = l[0]
            panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()

    panGenes = np.array(np.loadtxt(panGeneFile, skiprows=1, usecols = range(3, len(accession_dict)+3, 1), delimiter=',', dtype='str'))
    return panGenesId_dict, accession_dict, panGenes

maxInt = sys.maxsize
csv.field_size_limit(maxInt)
def readRilPanGene(rilPanGeneFile):
    ril_panGenesId_dict = dict()
    ril_accession_dict = dict()
    f1 = open(rilPanGeneFile, 'r')
    i = 0
    results = []
    for line in f1:
        line = line.strip()
        l = line.split()
        if i == 0:
            ii = -1
            for l1 in l:
                if ii >= 0:
                    ril_accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            results.append(line)
            pan_gene = l[0]
            ril_panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()
    # row_index = ril_panGenesId_dict[pan_gene_id] + 1
    # with open(rilPanGeneFile, 'r') as fin:
    #     reader=csv.reader(fin)
    #     result=[[s for s in row] for i,row in enumerate(reader) if i == row_index]
    #     print(result)
    return ril_accession_dict, ril_panGenesId_dict, results

#
# panGenesId_dict, accession_dict, panGenes = readPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv")
# fastas = readFastaFile( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/all.proteins.fas")
# fastasRev = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/unique.seqs.fa")
# fastasRev2 = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/unique.seqs.fa")
#
# with open("panGenesId_dict", 'wb') as f:
#     pickle.dump(panGenesId_dict, f, protocol=4)
#
# with open("accession_dict", 'wb') as f:
#     pickle.dump(accession_dict, f, protocol=4)
#
# with open("panGenes", 'wb') as f:
#     pickle.dump(panGenes, f, protocol=4)
#
# with open("fastas", 'wb') as f:
#     pickle.dump(fastas, f, protocol=4)
#
# with open("fastasRev", 'wb') as f:
#     pickle.dump(fastasRev, f, protocol=4)
#
# with open("fastasRev2", 'wb') as f:
#     pickle.dump(fastasRev2, f, protocol=4)


pan_group_ids = set()
gene_to_diversity = dict()
f1 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations/sequenceStructureDivergentMafftlDDT", 'r')
for line in f1:
    line = line.replace("\"", "")
    l = line.split()
    pan_group_ids.add(l[1])
#    print(l[0])
    gene_to_diversity[l[0]] = [float(l[2]), float(l[3]), float(l[4]), float(l[5]), float(l[6])]
#
# fastas = pickle.load(open("fastas", "rb"))
# fastasRev = pickle.load(open("fastasRev", "rb"))
# fastasRev2 = pickle.load(open("fastasRev2", "rb"))

accession_sum2 = dict()
accession_sum3 = dict()
accession_sum4 = dict()
accession_sum5 = dict()
accession_sum6 = dict()
print("line 179")
ril_accession_dict, ril_panGenesId_dict, results = readRilPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_genes_tables/mergedPanGeneTables")

for pan_group_id in pan_group_ids:
#    print("line 182")
    pan_gene_id = "pan_gene_" + pan_group_id
    print(pan_gene_id)
    pdbFiles = {}
    energyFiles = {}
    for accession in ril_accession_dict:
        if accession not in accession_sum2:
            accession_sum2[accession] = 0
            accession_sum3[accession] = 0
            accession_sum4[accession] = 0
            accession_sum5[accession] = 0
            accession_sum6[accession] = 0

#        print(ril_panGenesId_dict[pan_gene_id])
#        print(print(results[ril_panGenesId_dict[pan_gene_id]]))
        geneIds = results[ril_panGenesId_dict[pan_gene_id]].split()[ril_accession_dict[accession] + 1].split(";")
        id = -1
        geneId = "haha"
        while (id) < len(geneIds)-1 and (geneId not in gene_to_diversity):
            id = id + 1
            geneId = geneIds[id]
            geneId = geneId.replace("_T", "_P")
#            print("line 201\t" + geneId)

        if (id < len(geneIds)) and (geneId != "NA") and (geneId in gene_to_diversity):
 #           print(geneId)
            accession_sum2[accession] = accession_sum2[accession] + gene_to_diversity[geneId][0]
            accession_sum3[accession] = accession_sum3[accession] + gene_to_diversity[geneId][1]
            accession_sum4[accession] = accession_sum4[accession] + gene_to_diversity[geneId][2]
            accession_sum5[accession] = accession_sum5[accession] + gene_to_diversity[geneId][3]
            accession_sum6[accession] = accession_sum6[accession] + gene_to_diversity[geneId][4]

output = open("./SimilaritySumForEachAccessionMafftlDDT", 'w')
for accession in accession_sum2:
    output.write(accession + "\t" + str(accession_sum2[accession]) + "\t" + str(accession_sum3[accession]) + "\t" + str(accession_sum4[accession]) + "\t" + str(accession_sum5[accession]) + "\t" + str(accession_sum6[accession]) + "\n")
output.close()
