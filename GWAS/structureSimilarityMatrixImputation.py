from pathlib import Path
from ftplib import FTP
import prody as pd
from dataclasses import dataclass
import numpy as np
import typing as ty
from geometricus import MomentInvariants, SplitType
import tarfile
from time import time
from tqdm import tqdm
from scipy import ndimage
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
import itertools
from pathlib import Path
from sklearn.decomposition import NMF
import openTSNE
import os
import pickle
import re
import sys
from copy import deepcopy
import glob
# from src import uniprot_parser, proteinnet_parser
from sklearn.decomposition import TruncatedSVD
from os.path import exists
import csv
from statistics import mean
import scipy.stats as ss

def readFastaFile(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[name] = s
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[name] = s
    return fastas


def readFastaFileRev(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[s] = name
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[s] = name
    return fastas


def readPanGene(panGeneFile):
    panGenesId_dict = dict()
    accession_dict = dict()
    f1 = open(panGeneFile, 'r')
    i = 0
    for line in f1:
        line = line.strip()
        l = line.split(",")
        if i == 0:
            ii = -3
            for l1 in l:
                if ii >= 0 and ii<=25:
                    accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            pan_gene = l[0]
            panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()

    panGenes = np.array(np.loadtxt(panGeneFile, skiprows=1, usecols = range(3, len(accession_dict)+3, 1), delimiter=',', dtype='str'))
    return panGenesId_dict, accession_dict, panGenes

maxInt = sys.maxsize
csv.field_size_limit(maxInt)
def readRilPanGene(rilPanGeneFile):
    ril_panGenesId_dict = dict()
    ril_accession_dict = dict()
    f1 = open(rilPanGeneFile, 'r')
    i = 0
    results = []
    for line in f1:
        line = line.strip()
        l = line.split()
        if i == 0:
            ii = -1
            for l1 in l:
                if ii >= 0:
                    ril_accession_dict[l1] = ii
                ii = ii + 1
        elif i > 0:
            results.append(line)
            pan_gene = l[0]
            ril_panGenesId_dict[pan_gene] = i - 1
        i = i+1
    f1.close()
    # row_index = ril_panGenesId_dict[pan_gene_id] + 1
    # with open(rilPanGeneFile, 'r') as fin:
    #     reader=csv.reader(fin)
    #     result=[[s for s in row] for i,row in enumerate(reader) if i == row_index]
    #     print(result)
    return ril_accession_dict, ril_panGenesId_dict, results


def readSimilarityMatrix(similarityMatrix):
    genesId_dict = dict()
    f1 = open(similarityMatrix, 'r')
    i = 0
    results = []
    for line in f1:
        line = line.strip()
        results.append(line)
        l = line.split()
        genesId_dict[l[0]] = i
        i = i+1
    f1.close()
    return genesId_dict, results


pan_group_ids = set()
gene_to_diversity = dict()
f1 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations/sequenceStructureDivergent2", 'r')
for line in f1:
    line = line.replace("\"", "")
    l = line.split()
    pan_group_ids.add(l[1])
#    print(l[0])
    gene_to_diversity[l[0]] = [float(l[2]), float(l[3]), float(l[4]), float(l[5]), float(l[6])]
#
# fastas = pickle.load(open("fastas", "rb"))
# fastasRev = pickle.load(open("fastasRev", "rb"))
# fastasRev2 = pickle.load(open("fastasRev2", "rb"))

ril_accession_dict, ril_panGenesId_dict, results = readRilPanGene("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_genes_tables/mergedPanGeneTables")

for pan_group_id in pan_group_ids:
    genesId_dict, results = readSimilarityMatrix("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations/" + pan_group_id + "/lDDTmatrix")


    output = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/deleteriousMutations/" + pan_group_id + "/lDDTmatrix_panRIL", 'w')
    for accession in accession_sum2:
        output.write(accession + "\t" + str(accession_sum2[accession]) + "\t" + str(accession_sum3[accession]) + "\t" + str(accession_sum4[accession]) + "\t" + str(accession_sum5[accession]) + "\t" + str(accession_sum6[accession]) + "\n")
    output.close()
