"""
Created: 25/12/2017  00:40:39
Author: Baoxing Song
Email: songbaoxing168@163.com
This source code is partially adopted from https://github.com/bvilhjal/mixmogam/
And the R source code of emma is referred

modified: 31 Oct 2019
By Baoxing Song try to take the CNS present/absent as genotypic variant for GWAS analysis using a fixed model
about the f test implemented here https://towardsdatascience.com/fisher-test-for-regression-analysis-1e1687867259
"""
import os.path

import numpy as np
from numpy import linalg
from scipy import stats
import warnings
import re
import sys
import scipy.stats as ss
class LinearModel:
    """
    A class for linear models
    """
    def __init__(self, Y=None, dtype='float'):
        """
        The fixed effects should be a list of fixed effect lists (SNPs)
        """
        self.n = len(Y)
        self.Y = np.matrix(Y, dtype=dtype)
        self.Y.shape = (self.n, 1)
        self.X = np.matrix(np.ones((self.n, 1), dtype=dtype))  # The intercept
        self.p = 1

    def add_factor(self, x, lin_depend_thres=1e-4):
        """
        Adds an explanatory variable to the X matrix.
        """
        new_x = np.array(x, dtype='float')
        new_x.shape = len(x)
        if lin_depend_thres>0.0:
            # Checking whether this new cofactor in linearly independent.
            (beta, rss, rank, sigma) = linalg.lstsq(self.X, new_x)
            if float(rss) < lin_depend_thres:
                warnings.warn(
                    'A factor was found to be linearly dependent on the factors already in the X matrix.  Hence skipping it!')
                return False
        new_x.shape = (self.n, 1)
        self.X = np.hstack([self.X, new_x])
        self.p += 1
        return True

    def get_estimates_fix_model(self, xs=None, lin_depend_thres=1e-4):
        X = np.copy(self.X)
        if lin_depend_thres>0.0:
            for i in range(np.size(xs,1)):
                this_xs = xs[:, i]
                (beta, rss, rank, sigma) = linalg.lstsq(X, this_xs)
                if float(rss) > lin_depend_thres:
                    X = np.hstack([X, this_xs])
        else:
            X = np.hstack([self.X, xs])  # self.x is covariance, PCs or families ID. xs is structure

        q = X.shape[1]  # number of fixed effects
        n = self.n  # number of individuls
        p = n - q

        l = X.shape[1] - self.X.shape[1]
        if l == 0:
            return float(2.0)

        (beta_est, rss, rank, sigma) = linalg.lstsq(X, self.Y)  # residules using PCs + structure
        (h0_betas, h0_rss, h0_rank, h0_s) = linalg.lstsq(self.X, self.Y) # residules using PCs
        if len(rss) == 0:
            return float(2.0)
        if len(h0_rss) == 0:
            return float(2.0)
        if h0_rss == rss:
            return float(2.0)
        f_stat = (h0_rss / rss - 1) * p / l
        p_val = stats.f.sf(f_stat, l, p)
        if len(p_val) == 0:
            return float(2.0)
        p_val = float(p_val)
        return p_val

def parse_protein_structure(filename):
    energymin = {}
    shapermerSvd = {}
    with open(filename) as f:
        for line_i, line in enumerate(f):
            l = line.split()
            energymin[l[0]] = float(l[4])
            shapermerSvd[l[0]] = [float(l[2]), float(l[3])]
    return energymin, shapermerSvd

phenotype = sys.argv[1]
f = open(sys.argv[2], 'r')
line_index = 0
phen_index = {}  #key is the phenotype id, value is the column index
phen_individs = {} # key is the taxa name and value is the phenotype value

for line in f:
    if 0 == line_index:
        l = line.split(",")
        for i in range(0, len(l)):
            stripped_li = l[i].strip('"')
            phen_index[stripped_li] = i
    else:
        l = line.split(",")
        phen_individs[l[0].strip('"')] = float(l[phen_index[phenotype]])
    line_index = line_index + 1

f.close()
#read phenotype data end

energymin, shapermerSvd = parse_protein_structure(sys.argv[3])


pagene_id_to_B73 = dict()
f0 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/pan_gene_matrix_v3_cyverse.csv", 'r')
for line in f0:
    l = list(map(str.strip, line.split(",")))
    gene = l[3]
    gene = gene[:len(gene) - 5]
    pagene_id_to_B73[l[0]] = gene

expression_dict = dict()
if os.path.basename(sys.argv[3]) in pagene_id_to_B73:
    if os.path.exists("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[os.path.basename(sys.argv[3])]):
        f = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[os.path.basename(sys.argv[3])], 'r')
        for line in f:
            l = line.split()
            expression_dict[l[0]] = [float(l[1]), float(l[2]), float(l[3]), float(l[4]), float(l[5]), float(l[6]), float(l[6])]
#        print("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[os.path.basename(sys.argv[3])])
    else:
 #       print("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/imputed_exp_282_NAM_from282exp_NAMhaplotypes/expressionforeachGene/" + pagene_id_to_B73[os.path.basename(sys.argv[3])])
        sys.exit()
else:
    print(os.path.basename(sys.argv[3]) + " do not exists")
#
# get the PCS taxa id to line number map. line number could be queried from the genotypic variants

#pca reading end

# print ("begein association analysis")
lin_depend_thres=1e-4

this_families = []
this_energy=[]
this_shapmer=[]
this_phenotype = []
this_expression = []
for individual in phen_individs:
#    if (individual in energymin) and (individual in expression_dict):
    if (individual in energymin) and (individual in expression_dict):
        this_phenotype.append(phen_individs[individual])
        this_energy.append(energymin[individual])
        this_shapmer.append(shapermerSvd[individual])
        this_expression.append( expression_dict[individual] )
        if individual.startswith( 'Z001'):
            this_families.append( [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z002'):
            this_families.append( [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z003'):
            this_families.append( [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z004'):
            this_families.append( [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z005'):
            this_families.append( [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z006'):
            this_families.append( [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z007'):
            this_families.append( [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z008'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z009'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z010'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z011'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z012'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z013'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z014'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z015'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z016'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z018'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z019'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z020'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0] )
        elif individual.startswith( 'Z021'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0] )
        elif individual.startswith( 'Z022'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0] )
        elif individual.startswith( 'Z023'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0] )
        elif individual.startswith( 'Z024'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0] )
        elif individual.startswith( 'Z025'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1] )
        elif individual.startswith( 'Z026'):
            this_families.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )
        else:
            print("end of family, here is something wrong:" + individual)

# this_phenotype = np.array(this_phenotype)
# this_phenotype = this_phenotype - np.nanmin(this_phenotype)+0.01
# this_phenotype = (this_phenotype / np.amax(this_phenotype)) /1.01
# this_phenotype = ss.norm.ppf(this_phenotype)


lmm = LinearModel(this_phenotype)

this_pcs = np.mat(np.array(this_families), dtype='float')
# print(this_pcs.shape)
for i in range(np.size(this_pcs,1)):
    lmm.add_factor(this_pcs[:, i], lin_depend_thres=lin_depend_thres) # put pcs as cofactors

p_val = lmm.get_estimates_fix_model(xs=np.matrix(this_energy).T) # test folding energy
output = ""
if p_val <= 1.0:
    output = os.path.basename(sys.argv[3]) + "\t" + str(p_val)
else:
    output = os.path.basename(sys.argv[3]) + "\tNA"

p_val = lmm.get_estimates_fix_model(xs=np.matrix(this_shapmer)) # test folding energy
if p_val <= 1.0:
    output = output + "\t" + str(p_val)
else:
    output = output + "\tNA"

p_val = lmm.get_estimates_fix_model(xs=np.matrix(this_expression)) # test folding energy
if p_val <= 1.0:
    output = output + "\t" + str(p_val)
else:
    output = output + "\tNA"

xs = np.hstack([np.matrix(this_expression), np.matrix(this_energy).T, np.matrix(this_shapmer)])
p_val = lmm.get_estimates_fix_model(xs=xs) # test folding energy
if p_val <= 1.0:
    output = output + "\t" + str(p_val)
else:
    output = output + "\tNA"

this_expression = np.mat(np.array(this_expression), dtype='float')
for i in range(np.size(this_expression,1)):
    lmm.add_factor(this_expression[:, i], lin_depend_thres=lin_depend_thres) # put pcs as cofactors

xs = np.hstack([np.matrix(this_energy).T, np.matrix(this_shapmer)])
p_val = lmm.get_estimates_fix_model(xs=xs) # test folding energy
if p_val <= 1.0:
    output = output + "\t" + str(p_val)
else:
    output = output + "\tNA"
print(output)
