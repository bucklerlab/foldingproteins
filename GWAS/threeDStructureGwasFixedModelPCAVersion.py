"""
Created: 25/12/2017  00:40:39
Author: Baoxing Song
Email: songbaoxing168@163.com
This source code is partially adopted from https://github.com/bvilhjal/mixmogam/
And the R source code of emma is referred

modified: 31 Oct 2019
By Baoxing Song try to take the CNS present/absent as genotypic variant for GWAS analysis using a fixed model
about the f test implemented here https://towardsdatascience.com/fisher-test-for-regression-analysis-1e1687867259
"""
import os.path

import numpy as np
from numpy import linalg
from scipy import stats
import warnings
import re
import sys

class LinearModel:
    """
    A class for linear models
    """
    def __init__(self, Y=None, dtype='float'):
        """
        The fixed effects should be a list of fixed effect lists (SNPs)
        """
        self.n = len(Y)
        self.Y = np.matrix(Y, dtype=dtype)
        self.Y.shape = (self.n, 1)
        self.X = np.matrix(np.ones((self.n, 1), dtype=dtype))  # The intercept
        self.p = 1

    def add_factor(self, x, lin_depend_thres=1e-4):
        """
        Adds an explanatory variable to the X matrix.
        """
        new_x = np.array(x, dtype='float')
        new_x.shape = len(x)
        if lin_depend_thres>0.0:
            # Checking whether this new cofactor in linearly independent.
            (beta, rss, rank, sigma) = linalg.lstsq(self.X, new_x)
            if float(rss) < lin_depend_thres:
                warnings.warn(
                    'A factor was found to be linearly dependent on the factors already in the X matrix.  Hence skipping it!')
                return False
        new_x.shape = (self.n, 1)
        self.X = np.hstack([self.X, new_x])
        self.p += 1
        return True

    def get_estimates_fix_model(self, xs=None):
        X = np.hstack([self.X, xs])
        q = X.shape[1]  # number of fixed effects
        n = self.n  # number of individuls
        p = n - q
        (beta_est, rss, rank, sigma) = linalg.lstsq(X, self.Y)
        (h0_betas, h0_rss, h0_rank, h0_s) = linalg.lstsq(self.X, self.Y)
        if len(rss) == 0:
            return float(2.0)
        if len(h0_rss) == 0:
            return float(2.0)
        if h0_rss == rss:
            return float(2.0)
        f_stat = (h0_rss / rss - 1) * p / xs.shape[1]
        p_val = stats.f.sf(f_stat, (xs.shape[1]), p)
        if len(p_val) == 0:
            return float(2.0)
        p_val = float(p_val)
        return p_val

def parse_protein_structure(filename):
    energymin = {}
    shapermerSvd = {}
    with open(filename) as f:
        for line_i, line in enumerate(f):
            l = line.split()
            energymin[l[0]] = float(l[4])
            shapermerSvd[l[0]] = [float(l[2]), float(l[3])]
    return energymin, shapermerSvd

phenotype = sys.argv[1]
f = open(sys.argv[2], 'r')
line_index = 0
phen_index = {}  #key is the phenotype id, value is the column index
phen_individs = {} # key is the taxa name and value is the phenotype value

for line in f:
    if 0 == line_index:
        l = line.split(",")
        for i in range(0, len(l)):
            stripped_li = l[i].strip('"')
            phen_index[stripped_li] = i
    else:
        l = line.split(",")
        phen_individs[l[0].strip('"')] = float(l[phen_index[phenotype]])
    line_index = line_index + 1

f.close()
#read phenotype data end

energymin, shapermerSvd = parse_protein_structure(sys.argv[3])
#
# get the PCS taxa id to line number map. line number could be queried from the genotypic variants
pcs = np.mat(np.loadtxt("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/eigVectors", usecols = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25))).astype("float") # this is the pcs matrix

pcs_id = {} # key is the taxa name and value is the id of taxa in the matrix
f1 = open("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/canonicalPorteins/GWAS/eigVectors", 'r')
i = 0
for line in f1:
    l = list(map(str.strip, line.split()))
    pcs_id[l[0]] = i # taxa id and the index of taxa in the matrix
    i = i + 1

f1.close()
#pca reading end

# print ("begein association analysis")
lin_depend_thres=1e-4

this_pcs = []
this_energy=[]
this_shapmer=[]
this_phenotype = []
for individual in phen_individs:
    if (individual in pcs_id) and (individual in energymin):
#    if (individual in energymin):
        this_phenotype.append(phen_individs[individual])
        this_pcs.append( pcs[pcs_id[individual]] )
        this_energy.append(energymin[individual])
        this_shapmer.append(shapermerSvd[individual])

lmm = LinearModel(this_phenotype)

this_pcs = np.mat(np.array(this_pcs), dtype='float')
for i in range(np.size(this_pcs,1)):
    lmm.add_factor(this_pcs[:, i], lin_depend_thres=lin_depend_thres) # put pcs as cofactors

# print(this_phenotype)
# print(this_energy)

p_val = lmm.get_estimates_fix_model(xs=np.matrix(this_energy).T) # test folding energy
if p_val <= 1.0:
    print( os.path.basename(sys.argv[3]) + "\t" + str(p_val) )

# p_val = lmm.get_estimates_fix_model(xs=np.matrix(this_shapmer).T) # test folding energy
# if p_val <= 1.0:
#     print( os.path.basename(sys.argv[3]) + "\t" + str(p_val) )
