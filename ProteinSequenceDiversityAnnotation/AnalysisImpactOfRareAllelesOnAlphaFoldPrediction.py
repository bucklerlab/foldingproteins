#!python
import re
import pandas as pd
import copy
import glob
import twoPdbToMatrix
import MyUtil

# bs674@cornell.edu

def readFastaFile(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[name] = s
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[name] = s
    return fastas


def readFastaFileRev(fastaFile):
    fastas = {}
    name = ""
    seq = []
    with open(fastaFile) as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    fastas[s] = name
                name = m.group(1)
                name = name.replace("_P", "_T")
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            fastas[s] = name
    return fastas

def readEnergyFiles(energy_file):
    with open(energy_file) as f:
        for line in f:
            m = re.search('"final_energy": (\S+),', line)
            if (m != None):
                return float(m.group(1))

def readPdb(pdb_file, index):
    with open(pdb_file) as f:
        for line in f:
            elements = line.split()
            if (elements[2] == "CA" and int(elements[5]) == index):
                return float(line[60:66])

fastas = readFastaFile( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/all.proteins.fas")
fastasRev = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/unique.seqs.fa")
fastasRev2 = readFastaFileRev( "/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/unique.seqs.fa")

data = pd.read_csv('/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/DSSP.overlap.referAllelsAreAncestral', sep="\t", header=0)
for index, row in data.iterrows():
    proteinId = row['transcript']
    if row['SNP_type']=="nonsynonymous" and (proteinId in fastas) and row['altAA'][0]!='*':
        seq = fastas[proteinId]
        altSeq = seq[0:(int(row['codon_index'])-1)] + row['altAA'][0] + seq[int(row['codon_index']):]
        refPdb=""
        refEnergy = ""
        altPdb = ""
        altEnerty = ""
        if altSeq in fastasRev:
            if len(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[altSeq] + "/*maximumplddts.pdb")) >0:
                altPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[altSeq] + "/*maximumplddts.pdb")[0]
                altEnerty = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[altSeq] + "/*maximumplddts.energy")[0]
        elif altSeq in fastasRev2:
            if len(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[altSeq] + "/*maximumplddts.pdb")) >0:
                altPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[altSeq] + "/*maximumplddts.pdb")[0]
                altEnerty = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[altSeq] + "/*maximumplddts.energy")[0]
        if seq in fastasRev2:
            if len(glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[seq] + "/*maximumplddts.pdb")) >0:
                refPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[seq] + "/*maximumplddts.pdb")[0]
                refEnergy = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[seq] + "/*maximumplddts.energy")[0]

        if len(refPdb)>0 and len(altPdb)>0:
            refEnergyValue = readEnergyFiles(refEnergy)
            altEnergyValue = readEnergyFiles(altEnerty)
            refPlddt = readPdb(refPdb, int(row['codon_index']))
            altPlddt = readPdb(altPdb, int(row['codon_index']))
            dis = twoPdbToMatrix.calculateDistance(refPdb, altPdb)
            refASA = 0.0
            refPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[seq] + "/*mkdssp")[0]
            with open(refPdb) as pdbf:
                for pdbline in pdbf:
                    e = pdbline.split()
                    if(len(e) > 10 and e[0] == e[1]) and e[2]=="A" and int(e[0])==int(row['codon_index']):
                        refASA = float(pdbline[35:38])
            altAsa = 0.0
            altPdb = ""
            if altSeq in fastasRev:
                altPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/otherAccessions/" + fastasRev[altSeq] + "/*mkdssp")[0]
            elif altSeq in fastasRev2:
                altPdb = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + fastasRev2[altSeq] + "/*mkdssp")[0]

            with open(altPdb) as pdbf:
                for pdbline in pdbf:
                    e = pdbline.split()
                    if(len(e) > 10 and e[0] == e[1]) and e[2]=="A" and int(e[0])==int(row['codon_index']):
                        altAsa = float(pdbline[35:38])
            refMaxAsa = MyUtil.residue_max_acc[MyUtil.singleLetterAminoAcidToTripleLetterAminoAcid[row['refAA']]]
            altMaxAsa = MyUtil.residue_max_acc[MyUtil.singleLetterAminoAcidToTripleLetterAminoAcid[row['altAA']]]
            print( str(row["chr"]) + "\t" + str(row["position"]) + "\t" + row["transcript"] + "\t" + str(row["refFrq"]) + "\t" + str(row["altFrq"]) + "\t" + str(row["blosum62_score"]) + "\t" + str(refEnergyValue) + "\t" + str(altEnergyValue) +"\t"+ str(refPlddt) + "\t" + str(altPlddt) + "\t" + str(dis[0]) + "\t" + str(refASA) + "\t" + str(refMaxAsa) + "\t" + str(altAsa)+"\t" +str(altMaxAsa) + "\t" + str(row["blosum62_score"]))
