#!python
import numpy as np

import NucleotideCodeSubstitution
import FastaFile
import GffFile

# song@mpipz.mpg.de


def overlap_with_certain_transcript(position, chromosome_name, chromosome_transcript_list):
    for transcript_name in chromosome_transcript_list[chromosome_name]:
        if chromosome_transcript_list[chromosome_name][transcript_name].start > position:
            return None
        if chromosome_transcript_list[chromosome_name][transcript_name].end < position:
            continue
        if (chromosome_transcript_list[chromosome_name][transcript_name].start <= position) and (chromosome_transcript_list[chromosome_name][transcript_name].end >= position):
            return transcript_name
    return None

residue_max_acc = {"A": 129.0,
                   "R": 274.0,
                   "N": 195.0,
                   "D": 193.0,
                   "C": 167.0,
                   "Q": 225.0,
                   "E": 223.0,
                   "G": 104.0,
                   "H": 224.0,
                   "I": 197.0,
                   "L": 201.0,
                   "K": 236.0,
                   "M": 224.0,
                   "F": 240.0,
                   "P": 159.0,
                   "S": 155.0,
                   "T": 172.0,
                   "W": 285.0,
                   "Y": 263.0,
                   "V": 174.0,

                   "ALA": 129.0,
                   "ARG": 274.0,
                   "ASN": 195.0,
                   "ASP": 193.0,
                   "CYS": 167.0,
                   "GLN": 225.0,
                   "GLU": 223.0,
                   "GLY": 104.0,
                   "HIS": 224.0,
                   "ILE": 197.0,
                   "LEU": 201.0,
                   "LYS": 236.0,
                   "MET": 224.0,
                   "PHE": 240.0,
                   "PRO": 159.0,
                   "SER": 155.0,
                   "THR": 172.0,
                   "TRP": 285.0,
                   "TYR": 263.0,
                   "VAL": 174.0,
                   }

singleLetterAminoAcidToTripleLetterAminoAcid={
    "A": "ALA",
    "R": "ARG",
    "N": "ASN",
    "D": "ASP",
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU",
    "G": "GLY",
    "H": "HIS",
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL",
}
