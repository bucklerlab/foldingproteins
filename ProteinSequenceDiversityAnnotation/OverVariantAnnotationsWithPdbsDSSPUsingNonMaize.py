#!python

import NucleotideCodeSubstitution
import FastaFile
import re
import GffFile
import MyUtil
import math
import copy
import sys
import glob
from Bio.SubsMat import MatrixInfo

# bs674@cornell.edu

def score_match(pair, matrix):
    if pair not in matrix:
        return matrix[(tuple(reversed(pair)))]
    else:
        return matrix[pair]
_buckets = []

blosum = MatrixInfo.blosum62

def read_gvcf(index, gvcfFile, other_zea_variants):
    with open(gvcfFile) as f:
        for line in f:
            if line[0] != '#':
                element = line.split()
                start = int(element[1])
                if element[4][0] == '<':
                    end = int(element[7].split(";END=")[1])
                    for i in range(start, end+1):
                        if (element[0] in other_zea_variants) and (i in other_zea_variants[element[0]]):
                            other_zea_variants[element[0]][i][index] = "R"
                            if other_zea_variants[element[0]][i][0] == "N":
                                other_zea_variants[element[0]][i][0] = "R"
                elif (element[0] in other_zea_variants) and (start in other_zea_variants[element[0]]):
                    other_zea_variants[element[0]][start][index] = element[4].split(",")[0]
                    if other_zea_variants[element[0]][start][0] == "N":
                        other_zea_variants[element[0]][start][0] = element[3]
    return other_zea_variants

def read_data():
    protein_names, fastas = FastaFile.readFastaFile( "/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.protein.fa" )
    seq_to_id={}
    name = ""
    seq = []
    with open("/media/bs674/ppi8t/NAMpopulation_alphafold/B73V5/unique.seqs.fa") as f:
        for line in f:
            m = re.search('^>(\S+)', line)
            if (m != None):
                if (len(name) > 0) & (len(seq) > 0):
                    s = ''.join(seq)
                    s = re.sub("\\s", "", s)
                    s = s.upper()
                    seq_to_id[s]=name
                name = m.group(1)
                seq = []
            else:
                seq.append(line)
        if (len(name) > 0) & (len(seq) > 0):
            s = ''.join(seq)
            s = re.sub("\\s", "", s)
            s = s.upper()
            seq_to_id[s]=name

    wanted_dict={}
    other_zea_variants = {}
    with open("/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/B73andrelatedSpecies/snp.annotation") as f:
        for line in f:
            e = line.split()
            if e[1] not in wanted_dict:
                wanted_dict[e[1]] = set()
                other_zea_variants[e[1]] = {}
            wanted_dict[e[1]].add(e[2])
            other_zea_variants[e[1]][int(e[2])] = ["N", "N","N", "N", "N", "N", "N", "N", "N","N", "N", "N","N", "N", "N", "N", "N","N", "N","N", "N", "N","N", "N" ,"N" ,"N"]

    with open("/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/B97_anchorwave.gvcf") as f:
        for line in f:
            if line[0] != '#':
                element = line.split()
                start = int(element[1])
                if element[4][0] == '<':
                    end = int(element[7].split(";END=")[1])
                    for i in range(start, end+1):
                        if (element[0] in other_zea_variants) and (i in other_zea_variants[element[0]]):
                            other_zea_variants[element[0]][i][0] = "R"
                            other_zea_variants[element[0]][i][1] = "R"
#                            print(element[0] + "\t" + str(i) + "\t" + other_zea_variants[element[0]][i])
                elif (element[0] in other_zea_variants) and (start in other_zea_variants[element[0]]):
                    other_zea_variants[element[0]][start][0] = element[3]
                    other_zea_variants[element[0]][start][1] = element[4].split(",")[0]

    read_gvcf(2, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML247_anchorwave.gvcf", other_zea_variants)
    read_gvcf(3, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML333_anchorwave.gvcf", other_zea_variants)
    read_gvcf(4, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/HP301_anchorwave.gvcf", other_zea_variants)
    read_gvcf(5, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Ki3_anchorwave.gvcf", other_zea_variants)
    read_gvcf(6, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/M37W_anchorwave.gvcf", other_zea_variants)
    read_gvcf(7, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/NC350_anchorwave.gvcf", other_zea_variants)
    read_gvcf(8, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Oh7B_anchorwave.gvcf", other_zea_variants)
    read_gvcf(9, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Tzi8_anchorwave.gvcf", other_zea_variants)
    read_gvcf(10, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML103_anchorwave.gvcf", other_zea_variants)
    read_gvcf(11, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML277_anchorwave.gvcf", other_zea_variants)
    read_gvcf(12, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML52_anchorwave.gvcf", other_zea_variants)
    read_gvcf(13, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Il14H_anchorwave.gvcf", other_zea_variants)
    read_gvcf(14, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Ky21_anchorwave.gvcf", other_zea_variants)
    read_gvcf(15, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Mo18W_anchorwave.gvcf", other_zea_variants)
    read_gvcf(16, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/NC358_anchorwave.gvcf", other_zea_variants)
    read_gvcf(17, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/P39_anchorwave.gvcf", other_zea_variants)
    read_gvcf(18, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML228_anchorwave.gvcf", other_zea_variants)
    read_gvcf(19, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML322_anchorwave.gvcf", other_zea_variants)
    read_gvcf(20, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/CML69_anchorwave.gvcf", other_zea_variants)
    read_gvcf(21, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Ki11_anchorwave.gvcf", other_zea_variants)
    read_gvcf(22, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/M162W_anchorwave.gvcf", other_zea_variants)
    read_gvcf(23, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Ms71_anchorwave.gvcf", other_zea_variants)
    read_gvcf(24, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Oh43_anchorwave.gvcf", other_zea_variants)
    read_gvcf(25, "/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/Tx303_anchorwave.gvcf", other_zea_variants)


    gerpdict = {}
    with open("/media/bs674/ppi8t/NAMpopulation_alphafold/AGPv4/variantsannotation/GERP/GERP/Zea_mays.allChr.v5.bed") as f:
        for line in f:
            e = line.split()
            if (e[0] in wanted_dict) and (e[2] in wanted_dict[e[0]]):
                if e[0] not in gerpdict:
                    gerpdict[e[0]] = {}
                gerpdict[e[0]][e[2]] = e[3]

    with open("/media/bs674/ppi8t/NAMpopulation_alphafold/NAM_natural_variation/B73andrelatedSpecies/snp.annotation") as f:
        for line in f:
   #         print(line)
            [type, chr, position, allele_count, transcript_name, ref_allele, alt_allele, amino_acid_index, ref_frq, alt_frq, codon, altCodon, thisCdsPosition] = line.split()
            protein_name = transcript_name.replace("_T", "_P")
            if (protein_name in protein_names) and (fastas[protein_name] in seq_to_id):
                pdbFile = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + seq_to_id[fastas[protein_name]] + "/*mkdssp")
  #              print(pdbFile)
                if len(pdbFile) >0 :
                    pdbFile = pdbFile[0]
                    total_asa = 0.0
                    with open(pdbFile) as pdbf:
                        for pdbline in pdbf:
                            e = pdbline.split()
                            if(len(e) > 10 and e[0] == e[1]) and e[2]=="A" and int(e[0])==int(amino_acid_index):
#                                print(e)
                                total_asa = total_asa + float(pdbline[35:38])
 #                               print(pdbline[13:14].strip() + "\t" + NucleotideCodeSubstitution.middleStandardGeneticCode[codon])
                                assert  pdbline[13:14].strip() == NucleotideCodeSubstitution.middleStandardGeneticCode[codon], "not working"

                    plddt = 0.0
                    pdbFile2 = glob.glob("/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/" + seq_to_id[fastas[protein_name]] + "/*maximumplddts.pdb")[0]
                    with open(pdbFile2) as pdbf:
                        for pdbline in pdbf:
                            if (pdbline.startswith('ATOM')) and (int(pdbline[22:26].strip()) == int(amino_acid_index)):
                                plddt = float(pdbline[60:66])
                    #
                    # outAllele = ref_allele
                    # if (chr in other_zea_variants) and (int(position) in other_zea_variants[chr]):
                    #     if other_zea_variants[chr][int(position)] != "R:R":
                    #         outAllele = other_zea_variants[chr][int(position)].split(":")[1]
                    # else:
                    #     outAllele = "NA"

                    if NucleotideCodeSubstitution.middleStandardGeneticCode[codon] != '*':
                        maxAsa = MyUtil.residue_max_acc[MyUtil.singleLetterAminoAcidToTripleLetterAminoAcid[NucleotideCodeSubstitution.middleStandardGeneticCode[codon]]]
                        score = -20
                        if NucleotideCodeSubstitution.middleStandardGeneticCode[altCodon] != '*':
                            pair = (NucleotideCodeSubstitution.middleStandardGeneticCode[codon], NucleotideCodeSubstitution.middleStandardGeneticCode[altCodon])
                            score = score_match(pair, blosum)
                        gerp_score = "NA"
                        if (chr in gerpdict) and (position in gerpdict[chr]):
                            gerp_score = gerpdict[chr][position]

                        # print(seq_to_id[fastas[protein_name]].split("_")[0])
                        # print(seq_to_id[fastas[protein_name]].split("_")[1])
                        # print(other_zea_variants[chr][int(position)][0])
                        #
                        # print(other_zea_variants[chr][int(position)][1])
                        # print(other_zea_variants[chr][int(position)][2])
                        # print(other_zea_variants[chr][int(position)][3])
                        # print(other_zea_variants[chr][int(position)][4])

                        print(type + "\t" + chr + "\t" + position + "\t" + allele_count + "\t" + transcript_name + "\t" + seq_to_id[fastas[protein_name]].split("_")[0] + "\t" + seq_to_id[fastas[protein_name]].split("_")[1] + "\t" + ref_allele + "\t" + alt_allele  + "\t" + '\t'.join(other_zea_variants[chr][int(position)]) + "\t" + amino_acid_index + "\t" + ref_frq + "\t" + alt_frq + "\t" + codon + "\t" +  NucleotideCodeSubstitution.middleStandardGeneticCode[codon] + "\t" + altCodon + "\t" +  NucleotideCodeSubstitution.middleStandardGeneticCode[altCodon] + "\t" + str(score) + "\t" + thisCdsPosition + "\t" + gerp_score + "\t" + str(plddt) + "\t" + str(total_asa) + "\t" + str(maxAsa))
    #                            print(protein_name + "\t" + seq_to_id[fastas[protein_name]] + "\t" + pdbFile )

read_data()
