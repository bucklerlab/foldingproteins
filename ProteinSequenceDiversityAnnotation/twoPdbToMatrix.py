# Copyright 2021 DeepMind Technologies Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""lDDT protein distance score."""
import numpy as np
import re
import sys

# bs674@cornell.edu

# this function was copied from the alphafold source code

def lddt(predicted_points,
         true_points,
         true_points_mask,
         cutoff=15.,
         per_residue=False):
    """Measure (approximate) lDDT for a batch of coordinates.

    lDDT reference:
    Mariani, V., Biasini, M., Barbato, A. & Schwede, T. lDDT: A local
    superposition-free score for comparing protein structures and models using
    distance difference tests. Bioinformatics 29, 2722–2728 (2013).

    lDDT is a measure of the difference between the true distance matrix and the
    distance matrix of the predicted points.  The difference is computed only on
    points closer than cutoff *in the true structure*.

    This function does not compute the exact lDDT value that the original paper
    describes because it does not include terms for physical feasibility
    (e.g. bond length violations). Therefore this is only an approximate
    lDDT score.

    Args:
      predicted_points: (batch, length, 3) array of predicted 3D points
      true_points: (batch, length, 3) array of true 3D points
      true_points_mask: (batch, length, 1) binary-valued float array.  This mask
        should be 1 for points that exist in the true points.
      cutoff: Maximum distance for a pair of points to be included
      per_residue: If true, return score for each residue.  Note that the overall
        lDDT is not exactly the mean of the per_residue lDDT's because some
        residues have more contacts than others.

    Returns:
      An (approximate, see above) lDDT score in the range 0-1.
    """
    assert len(predicted_points.shape) == 3
    assert predicted_points.shape[-1] == 3
    assert true_points_mask.shape[-1] == 1
    assert len(true_points_mask.shape) == 3

    # Compute true and predicted distance matrices.
    dmat_true = np.sqrt(1e-10 + np.sum(
        (true_points[:, :, None] - true_points[:, None, :])**2, axis=-1))

    dmat_predicted = np.sqrt(1e-10 + np.sum(
        (predicted_points[:, :, None] -
         predicted_points[:, None, :])**2, axis=-1))

    dists_to_score = (
            (dmat_true < cutoff).astype(np.float32) * true_points_mask *
            np.transpose(true_points_mask, [0, 2, 1]) *
            (1. - np.eye(dmat_true.shape[1]))  # Exclude self-interaction.
    )

    # Shift unscored distances to be far away.
    dist_l1 = np.abs(dmat_true - dmat_predicted)

    # True lDDT uses a number of fixed bins.
    # We ignore the physical plausibility correction to lDDT, though.
    score = 0.25 * ((dist_l1 < 0.5).astype(np.float32) +
                    (dist_l1 < 1.0).astype(np.float32) +
                    (dist_l1 < 2.0).astype(np.float32) +
                    (dist_l1 < 4.0).astype(np.float32))

    # Normalize over the appropriate axes.
    reduce_axes = (-1,) if per_residue else (-2, -1)
    norm = 1. / (1e-10 + np.sum(dists_to_score, axis=reduce_axes))
    score = norm * (1e-10 + np.sum(dists_to_score * score, axis=reduce_axes))
    return score




class Coordinate:
    x = 0
    y = 0
    z = 0
    plddtScore = 0
    def __init__(self, x, y, z, plddtScore):
        self.x = x
        self.y = y
        self.z = z
        self.plddtScore = plddtScore


def readPdb(pdbFile):
    coordinates = {}
    with open(pdbFile) as f:
        for line in f:
            elements = line.split()
            if elements[0] == "ATOM":
                atomId = elements[2]
                aaid = elements[5]
                x = line[30:38]
                y = line[38:46]
                z = line[46:54]
                plddtScore = float(line[60:66])
                # print(x+"\t"+y+"\t"+z+"\t"+str(plddtScore))
                coordinate = Coordinate(x, y, z, plddtScore)
                if aaid not in coordinates:
                    coordinates[aaid] = {}
                coordinates[aaid][atomId] = coordinate
    return coordinates


def calculateDistance(pdbFileRef, pdbFileQuery):
    coordinatesRef = readPdb(pdbFileRef)
    coordinatesQuery = readPdb(pdbFileQuery)
    r = []
    q = []
    for refPosition in sorted(coordinatesRef):
        if coordinatesRef[str(refPosition)]["CA"].plddtScore >= 70.0 and coordinatesQuery[str(refPosition)]["CA"].plddtScore >= 70.0:
            r.append([coordinatesRef[str(refPosition)]["CA"].x, coordinatesRef[str(refPosition)]["CA"].y, coordinatesRef[str(refPosition)]["CA"].z])
            q.append([coordinatesQuery[str(refPosition)]["CA"].x, coordinatesQuery[str(refPosition)]["CA"].y, coordinatesQuery[str(refPosition)]["CA"].z])
    if len(r)==0 or len(q) ==0:
        return "NA"
    return lddt(np.array([q], dtype=np.float32), np.array([r], dtype=np.float32), np.array([[[1]] * len(r)], dtype=np.float32), cutoff=15., per_residue=False)


if __name__ == '__main__':
    pdbFileRef = "/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/972_5/relaxed_model_3_maximumplddts.pdb"
    pdbFileQuery = "/media/bs674/ppi8t/NAMpopulation_alphafold/predictedStructures/B73_result/972_4/relaxed_model_1_maximumplddts.pdb"
    dis = calculateDistance(pdbFileRef, pdbFileQuery)
    print(dis[0])
