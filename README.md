**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).



module load intel/19.1.1




singularity run --nv --bind /scratch1/06899/bs674/alphafold/docker:/mnt/fasta_path_0,/scratch1/06899/bs674/alphafold/scripts/databases/uniref90:/mnt/uniref90_database_path,/scratch1/06899/bs674/alphafold/scripts/databases/mgnify:/mnt/mgnify_database_path,/scratch1/06899/bs674/alphafold/scripts/databases
/uniclust30/uniclust30_2018_08:/mnt/uniclust30_database_path,/scratch1/06899/bs674/alphafold/scripts/databases/bfd:/mnt/bfd_database_path,/scratch1/06899/bs674/alphafold/scripts/databases/pdb70:/mnt/pdb70_database_path,/scratch1/06899/bs674/alphafold/scripts:/mnt/data_dir,/scratch1/06899/bs674/alphafold/scripts/datab
ases/pdb_mmcif:/mnt/template_mmcif_dir,/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif:/mnt/obsolete_pdbs_path,./tmp/alphafold:/mnt/output /work2/06899/bs674/alphafold2/alphafold/docker/alphafold2_latest.sif --fasta_paths=/mnt/fasta_path_0/B6J853.fasta --uniref90_database_path=/mnt/uniref90_database
_path/uniref90.fasta --mgnify_database_path=/mnt/mgnify_database_path/mgy_clusters.fa --uniclust30_database_path=/mnt/uniclust30_database_path/uniclust30_2018_08 --bfd_database_path=/mnt/bfd_database_path/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/mnt/pdb70_database_path/pdb70 --d
ata_dir=/mnt/data_dir/databases --template_mmcif_dir=/mnt/template_mmcif_dir/mmcif_files --obsolete_pdbs_path=/mnt/obsolete_pdbs_path/obsolete.dat --output_dir=/mnt/output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr'


module load intel/19.1.1
module load cuda/11.0
module load cudnn/8.0.5
module load nccl/2.8.3

unset PYTHONPATH
sh ./Miniconda3-latest-Linux-x86_64.sh -b -p /work2/06899/bs674/alphafold2/softwares/miniconda

#conda update -qy conda
unset PYTHONPATH
conda install -y -c conda-forge openmm=7.5.1 cudatoolkit==11.1.1 pdbfixer pip python=3.7

#pip3 install --upgrade pip
pip3 install -r /work2/06899/bs674/alphafold2/alphafold/requirements.txt
pip3 install --upgrade jax jaxlib==0.1.69+cuda111 -f https://storage.googleapis.com/jax-releases/jax_releases.html

patch -p0 < /work2/06899/bs674/alphafold2/alphafold/docker/openmm.patch

module load cuda/11.0
#module load intel/19.1.1
module load cudnn/8.0.5
module load nccl/2.8.3
export PATH=/work2/06899/bs674/alphafold2/softwares/bin:/work2/06899/bs674/software/bin:$PATH
//ln -s /opt/apps/cuda/11.0/lib64/libcusolver.so.10.5.0.218 libcusolver.so.11  # this import to solve the 'libcusolver.so.11' missing problem
export NVIDIA_VISIBLE_DEVICES=all
export TF_FORCE_UNIFIED_MEMORY=1
export XLA_PYTHON_CLIENT_MEM_FRACTION=4.0

CUDA_DEVICE_ORDER=PCI_BUS_ID
export CUDA_VISIBLE_DEVICES=1
export NVIDIA_VISIBLE_DEVICES=1

Could not load dynamic library 'libcusolver.so.11'; dlerror: libcusolver.so.11: cannot open shared object file: No such file or directory; LD_LIBRARY_PATH: /opt/apps/cuda11_0/nccl/2.8.3/lib:/opt/apps/cuda11_0/cudnn/8.0.5/lib64
:/opt/intel/compilers_and_libraries_2020.4.304/linux/mpi/intel64/libfabric/lib:/opt/intel/compilers_and_libraries_2020.4.304/linux/mpi/intel64/lib/release:/opt/intel/compilers_and_libraries_2020.4.304/linux/mpi/intel64/lib:/opt/intel/debugger_2020/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2020.1.217/linux
/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2020.1.217/linux/tbb/lib/intel64_lin/gcc4.8:/opt/intel/compilers_and_libraries_2020.1.217/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2020.1.217/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2020.1.217/linux/compiler/lib/intel64_lin:
/opt/apps/gcc/8.3.0/lib64:/opt/apps/gcc/8.3.0/lib:/opt/apps/cuda/11.0/lib64:/opt/apps/hwloc/1.11.12/lib:/opt/apps/pmix/3.1.4/lib




pip3 install tensorflow-gpu==2.5.0

python3 run_alphafold_predication.py --fasta_paths=/workdir/bs674/alphafold/A0A1D6PXQ9.fasta --uniref90_database_path=/workdir/bs674/alphafold/scripts/dataset/uniref90/uniref90.fasta --mgnify_database_path=/workdir/bs674/alphafold/scripts/dataset/mgnify/mgy_clusters_2018_12.fa --uniclust30_database_path=/workdir/bs674/alphafold/scripts/dataset/uniclust30/uniclust30_2018_08/uniclust30_2018_08 --bfd_database_path=/workdir/bs674/alphafold/scripts/dataset/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/workdir/bs674/alphafold/scripts/dataset/pdb70/pdb70 --data_dir=/workdir/bs674/alphafold/scripts/dataset --template_mmcif_dir=/workdir/bs674/alphafold/scripts/dataset/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/workdir/bs674/alphafold/scripts/dataset/pdb_mmcif/obsolete.dat --output_dir=./output --model_names=model_1,model_2,model_3,model_4,model_5,model_1_ptm,model_2_ptm,model_3_ptm,model_4_ptm,model_5_ptm --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/home/bs674/miniconda3/envs/alphafold/bin/jackhmmer --hhblits_binary_path=/home/bs674/miniconda3/envs/alphafold/bin/hhblits --hhsearch_binary_path=/home/bs674/miniconda3/envs/alphafold/bin/hhsearch --kalign_binary_path=/home/bs674/miniconda3/envs/alphafold/bin/kalign



wget https://git.scicore.unibas.ch/schwede/openstructure/-/raw/7102c63615b64735c4941278d92b554ec94415f8/modules/mol/alg/src/stereo_chemical_props.txt







wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-ppc64le.sh
sh ./Anaconda3-2021.05-Linux-ppc64le.sh  -b -p /scratch/06899/bs674/anaconda


sh ./Miniconda3-latest-Linux-ppc64le.sh -b -p /scratch/06899/bs674/miniconda
conda install -c anaconda cudnn
conda install -y -c conda-forge openmm=7.5.1 cudatoolkit==11.1.1 pdbfixer pip python=3.7
pip3 install -r /home/baoxing.song/requirements.txt
pip3 install --upgrade jax==0.2.17 jaxlib==0.1.69+cuda111 -f https://storage.googleapis.com/jax-releases/jax_releases.html
cd /90daydata/shared/baoxing.song/miniconda3/lib/python3.7/site-packages
patch -p0 < /90daydata/shared/baoxing.song/openmm.patch
conda install -c conda-forge tzdata
conda install -c bioconda hmmer
conda install -c etetoolkit kalign
#conda install -c conda-forge jaxlib



export TF_FORCE_UNIFIED_MEMORY=1
export XLA_PYTHON_CLIENT_MEM_FRACTION=4.0
CUDA_DEVICE_ORDER=PCI_BUS_ID
export CUDA_VISIBLE_DEVICES=1
export NVIDIA_VISIBLE_DEVICES=1


export TF_FORCE_UNIFIED_MEMORY=1
export XLA_PYTHON_CLIENT_MEM_FRACTION=4.0
CUDA_DEVICE_ORDER=PCI_BUS_ID
export CUDA_VISIBLE_DEVICES=0
export NVIDIA_VISIBLE_DEVICES=0


