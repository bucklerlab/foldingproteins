# Installation

The code used here is modified from code downloaded from github about 40 minutus after the first release on July 15th 2021.
The aims of those changes include:
1) Run CPU intensive jobs and GPU intensive jobs separately on different type of machines/nodes, to maximum hardware usage efficiency.
2) Copy database to RAM, to significantly reduce IO load.
3) For the relax step, only use the model with highest plDDT score. So that reduce CPU cost by 80% for this step, compared to the default settings.

Those changes could reduce computational resource cost, while do NOT change result.

## Download databases
Run the script `scripts/download_all_data.sh` to downlad the databases, as explained by the AlphaFold document.

## Installation dependencies
Install [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) for GPU support.

Those settings are specific to TACC Frontera
```
module load intel/19.1.1
module load cuda/11.0
module load cudnn/8.0.5
module load nccl/2.8.3
unset PYTHONPATH
```

```
# download miniconda
sh ./Miniconda3-latest-Linux-x86_64.sh -b -p miniconda

#conda update -qy conda
unset PYTHONPATH
conda install -y -c conda-forge openmm=7.5.1 cudatoolkit==11.1.1 pdbfixer pip python=3.7


# requirements.txt is a text file avaliable from the source code
pip3 install -r requirements.txt
pip3 install --upgrade jax jaxlib==0.1.69+cuda111 -f https://storage.googleapis.com/jax-releases/jax_releases.html
# openmm.patch is avaliable from the source code
patch -p0 < openmm.patch
```


# Run AlphaFold
All the B73 protein sequence, all the canonical porteins were de-duplicated and save as file B73unique.seqs.fa and NAM.unique.seqs.fa. All the protein foldings were performed taking those files as input. \
In practice, we put 5,000 sequences as a batch. 
## Sequence alignment
I divided the sequence alignment step into 4 commands, and run them using 3 slurm scripts. \
I ran those 3 scripts in sequential order. If the previous script was not finished, the next script would try to run the previous task, but that would increase the IO load. \
Those script should be ran on CPU machines.
### Align against mgnify and uniref90 database
```
#!/bin/bash
#SBATCH -J 00.alignment  # Job name
#SBATCH -o  sequenceAlignment.log                  # Name of stdout output file (%j expands to jobId)
#SBATCH -e sequenceAlignment.err       # Name of stderr error file
#SBATCH -p small                     # Queue name
#SBATCH -A FTA-SUB-Cornell           # project name
#SBATCH -N 1                          # Total number of nodes requested (56 cores/node)
#SBATCH -n 1                          # Total number of mpi tasks requested
#SBATCH -t 48:00:00                   # Run time (hh:mm:ss) ( 48 hours)
#SBATCH --mail-type=all    # Send email at begin and end of job
#SBATCH --mail-user=bs674@cornell.edu
export PATH=/work2/06899/bs674/software/bin:$PATH
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_cs219.ff* /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffdata /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/mgnify/mgy_clusters_2018_12.fa /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniref90/uniref90.fasta /dev/shm
parallel -j 40 < /work2/06899/bs674/alphafold2/com_mgnify00
rm /dev/shm/mgy_clusters_2018_12.fa
rm /dev/shm/uniref90.fasta
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/mgnify/mgy_clusters_2018_12.fa /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/uniref90/uniref90.fasta /dev/shm
parallel -j 40 < /work2/06899/bs674/alphafold2/com_uniref9000
```
With /work2/06899/bs674/alphafold2/com_mgnify00, there are 5,000 commands like:
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_mgnify.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_mgnify.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_mgnify.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_1 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_mgnify.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_2 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_mgnify.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_3 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```

With /work2/06899/bs674/alphafold2/com_uniref9000, there are 5,000 commands like this
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_uniref90.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_uniref90.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_uniref90.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_1 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_uniref90.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_2 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_uniref90.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_3 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```

### Align against pdb70 using hhsearch

```
#!/bin/bash
#SBATCH -J hhsearch_00  # Job name
#SBATCH -o  sequenceAlignment.log                  # Name of stdout output file (%j expands to jobId)
#SBATCH -e sequenceAlignment.err       # Name of stderr error file
#SBATCH -p small                     # Queue name
#SBATCH -A FTA-SUB-Cornell           # project name
#SBATCH -N 1                          # Total number of nodes requested (56 cores/node)
#SBATCH -n 1                          # Total number of mpi tasks requested
#SBATCH -t 48:00:00                   # Run time (hh:mm:ss) ( 48 hours)
#SBATCH --mail-type=all    # Send email at begin and end of job
#SBATCH --mail-user=bs674@cornell.edu
export PATH=/work2/06899/bs674/software/bin:$PATH
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_cs219.ff* /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffindex /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffdata /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffindex /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffindex /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffdata /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffindex /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/mgnify/mgy_clusters_2018_12.fa /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniref90/uniref90.fasta /dev/shm
parallel -j 24 < /work2/06899/bs674/alphafold2/com_hhsearch00
```

Within /work2/06899/bs674/alphafold2/com_hhsearch00, there are 5,000 commands like this
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_hhsearch.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_hhsearch.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_hhsearch.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_1 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_hhsearch.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_2 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment_hhsearch.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/1000_3 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=casp14 --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```

### align against bfd using hhblits
```
#!/bin/bash
#SBATCH -J 00.align  # Job name
#SBATCH -o sequenceAlignment.log                  # Name of stdout output file (%j expands to jobId)
#SBATCH -e sequenceAlignment.err       # Name of stderr error file
#SBATCH -p small                     # Queue name
#SBATCH -A FTA-SUB-Cornell           # project name
#SBATCH -N 1                          # Total number of nodes requested (56 cores/node)
#SBATCH -n 100                          # Total number of mpi tasks requested
#SBATCH -t 48:00:00                   # Run time (hh:mm:ss) ( 48 hours)
#SBATCH --mail-type=all    # Send email at begin and end of job
#SBATCH --mail-user=bs674@cornell.edu
export PATH=/work2/06899/bs674/software/bin:$PATH
cp /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_cs219.ff* /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffindex /dev/shm/
cp /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffdata /dev/shm
cp /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/mgnify/mgy_clusters_2018_12.fa /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniref90/uniref90.fasta /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/small_bfd/bfd-first_non_consensus_sequences.fasta /dev/shm
parallel -j 15 < /work2/06899/bs674/alphafold2/comm_align00
```

Within /work2/06899/bs674/alphafold2/comm_align00, there are 5,000 commands like this
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_1 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_10 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_100 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_sequence_alignment.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_101 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=full_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```

## Perform predication
Run this script on GPU nodes
```
#!/bin/bash
#SBATCH -J 00.prediction  # Job name
#SBATCH -o sequenceprediction00.log                  # Name of stdout output file (%j expands to jobId)
#SBATCH -e sequencePrediction00.err       # Name of stderr error file
#SBATCH -p rtx                     # Queue name
#SBATCH -A FTA-SUB-Cornell           # project name
#SBATCH -N 2                          # Total number of nodes requested (56 cores/node)
#SBATCH -n 8                          # Total number of mpi tasks requested
#SBATCH -t 48:00:00                   # Run time (hh:mm:ss) ( 48 hours)
#SBATCH --mail-type=all    # Send email at begin and end of job
#SBATCH --mail-user=bs674@cornell.edu
export PATH=/work2/06899/bs674/software/bin:$PATH
module load launcher_gpu
module load cuda/11.0
module load cudnn/8.0.5
module load nccl/2.8.3
export PATH=/work2/06899/bs674/alphafold2/softwares/bin:/work2/06899/bs674/software/bin:$PATH
export LAUNCHER_WORKDIR=/work2/06899/bs674/alphafold2
export LAUNCHER_JOB_FILE=comm_prediction00
${LAUNCHER_DIR}/paramrun
```

Within comm_prediction00, there are 5,000 commands like this
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_predication.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/scratch2/projects/bio/alphafold/data/uniref90/uniref90.fasta --mgnify_database_path=/scratch2/projects/bio/alphafold/data/mgnify/mgy_clusters_2018_12.fa  --small_bfd_database_path=/scratch2/projects/bio/alphafold/data/small_bfd/bfd-first_non_consensus_sequences.fasta --pdb70_database_path=/scratch2/projects/bio/alphafold/data/pdb70/pdb70 --data_dir=/scratch2/projects/bio/alphafold/data/ --template_mmcif_dir=/scratch2/projects/bio/alphafold/data/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch2/projects/bio/alphafold/data/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_predication.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_1 --uniref90_database_path=/scratch2/projects/bio/alphafold/data/uniref90/uniref90.fasta --mgnify_database_path=/scratch2/projects/bio/alphafold/data/mgnify/mgy_clusters_2018_12.fa  --small_bfd_database_path=/scratch2/projects/bio/alphafold/data/small_bfd/bfd-first_non_consensus_sequences.fasta --pdb70_database_path=/scratch2/projects/bio/alphafold/data/pdb70/pdb70 --data_dir=/scratch2/projects/bio/alphafold/data/ --template_mmcif_dir=/scratch2/projects/bio/alphafold/data/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch2/projects/bio/alphafold/data/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_predication.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_10 --uniref90_database_path=/scratch2/projects/bio/alphafold/data/uniref90/uniref90.fasta --mgnify_database_path=/scratch2/projects/bio/alphafold/data/mgnify/mgy_clusters_2018_12.fa  --small_bfd_database_path=/scratch2/projects/bio/alphafold/data/small_bfd/bfd-first_non_consensus_sequences.fasta --pdb70_database_path=/scratch2/projects/bio/alphafold/data/pdb70/pdb70 --data_dir=/scratch2/projects/bio/alphafold/data/ --template_mmcif_dir=/scratch2/projects/bio/alphafold/data/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch2/projects/bio/alphafold/data/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_predication.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_100 --uniref90_database_path=/scratch2/projects/bio/alphafold/data/uniref90/uniref90.fasta --mgnify_database_path=/scratch2/projects/bio/alphafold/data/mgnify/mgy_clusters_2018_12.fa  --small_bfd_database_path=/scratch2/projects/bio/alphafold/data/small_bfd/bfd-first_non_consensus_sequences.fasta --pdb70_database_path=/scratch2/projects/bio/alphafold/data/pdb70/pdb70 --data_dir=/scratch2/projects/bio/alphafold/data/ --template_mmcif_dir=/scratch2/projects/bio/alphafold/data/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch2/projects/bio/alphafold/data/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_predication.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_101 --uniref90_database_path=/scratch2/projects/bio/alphafold/data/uniref90/uniref90.fasta --mgnify_database_path=/scratch2/projects/bio/alphafold/data/mgnify/mgy_clusters_2018_12.fa  --small_bfd_database_path=/scratch2/projects/bio/alphafold/data/small_bfd/bfd-first_non_consensus_sequences.fasta --pdb70_database_path=/scratch2/projects/bio/alphafold/data/pdb70/pdb70 --data_dir=/scratch2/projects/bio/alphafold/data/ --template_mmcif_dir=/scratch2/projects/bio/alphafold/data/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch2/projects/bio/alphafold/data/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```


## Perform relax
Run this script on CPU node. \
This script only perform relaxation for the model having the highest plDDT score. \
This script could use all the available CPU threads automatically.
```
#!/bin/bash
#SBATCH -J 00.align  # Job name
#SBATCH -o  sequenceAlignment.log                  # Name of stdout output file (%j expands to jobId)
#SBATCH -e sequenceAlignment.err       # Name of stderr error file
#SBATCH -p normal                     # Queue name
#SBATCH -A FTA-SUB-Cornell           # project name
#SBATCH -N 1                          # Total number of nodes requested (56 cores/node)
#SBATCH -n 1                          # Total number of mpi tasks requested
#SBATCH -t 48:00:00                   # Run time (hh:mm:ss) ( 48 hours)
#SBATCH --mail-type=all    # Send email at begin and end of job
#SBATCH --mail-user=bs674@cornell.edu
export PATH=/work2/06899/bs674/software/bin:$PATH
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_cs219.ff* /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_a3m.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffdata /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_hhm.ffindex /dev/shm/
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_cs219.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_hhm.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffindex /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/pdb70/pdb70_a3m.ffdata /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/mgnify/mgy_clusters_2018_12.fa /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/uniref90/uniref90.fasta /dev/shm
ln -s /scratch1/06899/bs674/alphafold/scripts/databases/small_bfd/bfd-first_non_consensus_sequences.fasta /dev/shm
parallel -j 1 < /work2/06899/bs674/alphafold2/relax/comm_relax00
```
Within comm_relax00, there are 5,000 commands like this
```
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_relax_onlyrelaxMaximumplddts.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_0 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_relax_onlyrelaxMaximumplddts.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_1 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_relax_onlyrelaxMaximumplddts.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_10 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_relax_onlyrelaxMaximumplddts.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_100 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
python3 /work2/06899/bs674/alphafold2/alphafold/run_alphafold_relax_onlyrelaxMaximumplddts.py --fasta_paths=/work2/06899/bs674/alphafold2/unique.seqs/100_101 --uniref90_database_path=/dev/shm/uniref90.fasta --mgnify_database_path=/dev/shm/mgy_clusters_2018_12.fa --uniclust30_database_path=/dev/shm/uniclust30_2018_08 --bfd_database_path=/dev/shm/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --pdb70_database_path=/dev/shm/pdb70 --data_dir=/scratch1/06899/bs674/alphafold/scripts/databases --template_mmcif_dir=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/mmcif_files --obsolete_pdbs_path=/scratch1/06899/bs674/alphafold/scripts/databases/pdb_mmcif/obsolete.dat --output_dir=/scratch1/06899/bs674/unique.seqs.output --model_names=model_1,model_2,model_3,model_4,model_5 --max_template_date=2020-05-14 --preset=reduced_dbs --benchmark=False --logtostderr --jackhmmer_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/jackhmmer --hhblits_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhblits --hhsearch_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/hhsearch --kalign_binary_path=/work2/06899/bs674/alphafold2/softwares/bin/kalign
```