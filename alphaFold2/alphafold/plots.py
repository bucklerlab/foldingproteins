import sys
import os
from urllib import request
from concurrent import futures
import json
import matplotlib.pyplot as plt
import numpy as np
import py3Dmol
import pickle
from alphafold.relax import utils


#features = pickle.load(open("/media/bs674/ppi8t/NAMpopulation_alphafold/pangenome/sugar1/sugar1/mo18w/features.pkl", "rb"))

features = pickle.load(open(sys.argv[1], "rb"))

plddts = {}
pae_outputs = {}
model_name = '5'
prediction_result = pickle.load(open(sys.argv[2], "rb")) #pickle.load(open("/media/bs674/ppi8t/NAMpopulation_alphafold/pangenome/sugar1/sugar1/mo18w/result_model_2_ptm.pkl", "rb"))

mean_plddt = prediction_result['plddt'].mean()
print(mean_plddt)
if 'predicted_aligned_error' in prediction_result:
    pae_outputs[model_name] = (
        prediction_result['predicted_aligned_error'],
        prediction_result['max_predicted_aligned_error']
    )
else:
    # Get the pLDDT confidence metrics. Do not put pTM models here as they
    # should never get selected.
    plddts[model_name] = prediction_result['plddt']


plt.subplot(1, 2, 2)
pae, max_pae = list(pae_outputs.values())[0]
plt.imshow(pae, vmin=0., vmax=max_pae, cmap='Greens_r')
plt.colorbar(fraction=0.046, pad=0.04)
plt.title('Predicted Aligned Error')
plt.xlabel('Scored residue')
plt.ylabel('Aligned residue')
plt.savefig("result_model_2_ptm.pae.png")

rounded_errors = np.round(pae.astype(np.float64), decimals=1)
indices = np.indices((len(rounded_errors), len(rounded_errors))) + 1
indices_1 = indices[0].flatten().tolist()
indices_2 = indices[1].flatten().tolist()
pae_data = json.dumps([{
    'residue1': indices_1,
    'residue2': indices_2,
    'distance': rounded_errors.flatten().tolist(),
    'max_predicted_aligned_error': max_pae.item()
}],
    indent=None,
    separators=(',', ':'))
pae_output_path= "result_model_3_ptm.pae"
with open(pae_output_path, 'w') as f:
    f.write(pae_data)


PLDDT_BANDS = [(0, 50, '#FF7D45'),
               (50, 70, '#FFDB13'),
               (70, 90, '#65CBF3'),
               (90, 100, '#0053D6')]


banded_b_factors = []
for plddt in prediction_result['plddt']:
    for idx, (min_val, max_val, _) in enumerate(PLDDT_BANDS):
        if plddt >= min_val and plddt <= max_val:
            banded_b_factors.append(idx)
            break
final_atom_mask = prediction_result['structure_module']['final_atom_mask']
banded_b_factors = np.array(banded_b_factors)[:, None] * final_atom_mask
#relaxed_pdb=open('/media/bs674/ppi8t/NAMpopulation_alphafold/pangenome/sugar1/sugar1/mo18w/unrelaxed_model_2.pdb', 'r').read()
#to_visualize_pdb = utils.overwrite_b_factors(relaxed_pdb, banded_b_factors)

show_sidechains = True
def plot_plddt_legend():
    """Plots the legend for pLDDT."""
    thresh = [
        'Very low (pLDDT < 50)',
        'Low (70 > pLDDT > 50)',
        'Confident (90 > pLDDT > 70)',
        'Very high (pLDDT > 90)']

    colors = [x[2] for x in PLDDT_BANDS]

    plt.figure(figsize=(2, 2))
    for c in colors:
        plt.bar(0, 0, color=c)
    plt.legend(thresh, frameon=False, loc='center', fontsize=20)
    plt.xticks([])
    plt.yticks([])
    ax = plt.gca()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    plt.title('Model Confidence', fontsize=20, pad=20)
    return plt

# Color the structure by per-residue pLDDT
color_map = {i: bands[2] for i, bands in enumerate(PLDDT_BANDS)}
#view = py3Dmol.view(width=800, height=600)
#view.show()
#view.addModelsAsFrames(to_visualize_pdb)
#view.show()
style = {'cartoon': {
    'colorscheme': {
        'prop': 'b',
        'map': color_map}
}}
if show_sidechains:
    style['stick'] = {}
#view.setStyle({'model': -1}, style)
#view.zoomTo()

print("line 124")
#view.show()
print("line 126")
#plot_plddt_legend().show()
print("line 128")
#view.savefig("result_model_2_ptm.png")

plddt = prediction_result['plddt']

plt.figure(figsize=[8 * 1, 6])
plt.subplot(1, 1, 1)
plt.plot(plddt)
plt.title('Predicted LDDT')
plt.xlabel('Residue')
plt.ylabel('pLDDT')
#plt.show()
plt.savefig("result_model_2_ptm.lDDT.png")


#features = pickle.load(open("/media/bs674/ppi8t/NAMpopulation_alphafold/pangenome/sugar1/sugar1/mo18w/features.pkl", "rb"))
features = pickle.load(open(sys.argv[1], "rb"))

deduped_full_msa = features['msas']
full_msa = []
full_msa.extend(deduped_full_msa[0])
full_msa.extend(deduped_full_msa[1])
full_msa.extend(deduped_full_msa[2])
deduped_full_msa = list(dict.fromkeys(full_msa))

total_msa_size = len(deduped_full_msa)
print(f'\n{total_msa_size} Sequences Found in Total\n')

aa_map = {restype: i for i, restype in enumerate('ABCDEFGHIJKLMNOPQRSTUVWXYZ-')}
msa_arr = np.array([[aa_map[aa] for aa in seq] for seq in deduped_full_msa])
num_alignments, num_res = msa_arr.shape

fig = plt.figure(figsize=(12, 3))
plt.title('Per-Residue Count of Non-Gap Amino Acids in the MSA')
plt.plot(np.sum(msa_arr != aa_map['-'], axis=0), color='black')
plt.ylabel('Non-Gap Count')
plt.yticks(range(0, num_alignments + 1, max(1, int(num_alignments / 3))))
#plt.show()
plt.savefig("result_model_2_ptm.msa.depth.png")



msa_arr = np.array([list(seq) for seq in deduped_full_msa])
seqid = (np.array(msa_arr[0]) == msa_arr).mean(-1)
seqid_sort = seqid.argsort() #[::-1]
non_gaps = (msa_arr != "-").astype(float)
non_gaps[non_gaps == 0] = np.nan

##################################################################
plt.figure(figsize=(14,4),dpi=100)
##################################################################
plt.subplot(1,2,1); plt.title("Sequence coverage")
plt.imshow(non_gaps[seqid_sort]*seqid[seqid_sort,None],
           interpolation='nearest', aspect='auto',
           cmap="rainbow_r", vmin=0, vmax=1, origin='lower')
plt.plot((msa_arr != "-").sum(0), color='black')
plt.xlim(-0.5,msa_arr.shape[1]-0.5)
plt.ylim(-0.5,msa_arr.shape[0]-0.5)
plt.colorbar(label="Sequence identity to query",)
plt.xlabel("Positions")
plt.ylabel("Sequences")
plt.savefig("result_model_2_ptm.msa.depth.v2.png")
