import me.songbx.service.ProteinMsaReadService;


/**
 * @author song
 * @version 1.0, 2018-09-15
 */


public class DiversityChecking {
	public static void calProteinPi( String inputFile){
        ProteinMsaReadService proteinMsaReadService = new ProteinMsaReadService(inputFile);

		double innerPi = 0.0;
		double innerCapitalePi = 0.0;
		double innerValidatedLength=0.0;
		
		double interPi = 0.0;
		double interCapitalePi = 0.0;
		double interValidatedLength=0.0;
		
		for(int i=0; i < proteinMsaReadService.getProteinSequences().size(); i++){
			for(int j=i+1; j < proteinMsaReadService.getProteinSequences().size(); j++){
				if( proteinMsaReadService.getProteinSequences().get(i).getSpecies().equals( proteinMsaReadService.getProteinSequences().get(j).getSpecies() ) ) {			
					for(int k=0; k < proteinMsaReadService.getProteinSequences().get(i).getSequence().length(); k++){
						if( proteinMsaReadService.getProteinSequences().get(i).getSequence().charAt(k) != '-' && proteinMsaReadService.getProteinSequences().get(j).getSequence().charAt(k) != '-' ) {
							++innerValidatedLength;
							if( proteinMsaReadService.getProteinSequences().get(i).getSequence().charAt(k) != proteinMsaReadService.getProteinSequences().get(j).getSequence().charAt(k) ) {
								innerCapitalePi++;
							}
						}
					}
				} else {
					for(int k=0; k < proteinMsaReadService.getProteinSequences().get(i).getSequence().length(); k++){
						if( proteinMsaReadService.getProteinSequences().get(i).getSequence().charAt(k) != '-' && proteinMsaReadService.getProteinSequences().get(j).getSequence().charAt(k) != '-' ) {
							++interValidatedLength;
							if( proteinMsaReadService.getProteinSequences().get(i).getSequence().charAt(k) != proteinMsaReadService.getProteinSequences().get(j).getSequence().charAt(k) ) {
								interCapitalePi++;
							}
						}
					}
				}	
			}
		}
		innerPi = innerCapitalePi/innerValidatedLength;
		interPi = interCapitalePi/interValidatedLength;
		System.out.println(inputFile + "\t" + innerPi + "\t" + interPi);
	}
}
