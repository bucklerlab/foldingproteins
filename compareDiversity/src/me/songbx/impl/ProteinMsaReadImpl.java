package me.songbx.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.songbx.model.Sequence;

/**
 * @author song
 * @version 1.0, 2014-07-09
 */
public class ProteinMsaReadImpl {
	/**hashmap of chromosomes, the index are the names of chromosomes, the value are the chromosomes*/
	private ArrayList<Sequence> proteinSequences = new ArrayList<Sequence>();
	public ProteinMsaReadImpl() {
		
	}
	/**
	 * @param the location of fasta or multiple fasta format file. The meta-line should be like ">Chr2"
	 */
	public ProteinMsaReadImpl(String fileLocation) {
		File file = new File(fileLocation);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			StringBuffer sequenceBuffer = new StringBuffer();
			String sequenceName = "";
			String species = "";
			while ((tempString = reader.readLine()) != null) {
				if ( tempString.charAt(0) =='>' ) {
					if (sequenceName.length() > 0 && sequenceBuffer.length() > 0) {
						String seq = sequenceBuffer.toString().toUpperCase();
						seq=seq.replaceAll("\\s", "");
						Sequence c = new Sequence(sequenceName, seq, species);
						proteinSequences.add(c);
					}
					tempString = tempString.replaceAll("^>", "");
					sequenceName = tempString.split("\\s")[0];
					species = tempString.split("\\s")[1];
					sequenceBuffer = new StringBuffer();
					//System.out.println(sequenceName);
				} else {
					sequenceBuffer.append(tempString);
				}
			}
			if (sequenceName.length() > 0 && sequenceBuffer.length() > 0) {
				String seq = sequenceBuffer.toString().toUpperCase();
				seq=seq.replaceAll("\\s", "");
				Sequence c = new Sequence(sequenceName, seq, species);
				proteinSequences.add(c);
				//System.out.println(sequenceName);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {

				}
			}
		}
	}
	
	public ArrayList<Sequence> getProteinSequences() {
		return proteinSequences;
	}
	public void setProteinSequences(ArrayList<Sequence> proteinSequences) {
		this.proteinSequences = proteinSequences;
	}
	public static void main( String[] argv ){
		ProteinMsaReadImpl proteinMsaReadImpl = new ProteinMsaReadImpl("/media/bs674/ppi8t/NAMpopulation_alphafold/allNAMs/paralogousOrthologousDiversityComparsion/MSAresult/pan_gene_3.fa.msa");
		for ( Sequence proteinSequence : proteinMsaReadImpl.getProteinSequences() ) {
			System.out.println(proteinSequence.getName() + "\t" + proteinSequence.getSequence() + "\t" + proteinSequence.getSpecies());
		}
	}
}
