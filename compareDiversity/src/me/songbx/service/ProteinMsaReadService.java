package me.songbx.service;


import java.util.ArrayList;

import me.songbx.impl.ProteinMsaReadImpl;
import me.songbx.model.Sequence;

/**
 * @author song
 * @version 1.0, 2014-07-09
 */
public class ProteinMsaReadService {

	/**
	 * @param the location of fasta or multiple fasta format file. The meta-line should be like ">Chr2"
	 */
	private ProteinMsaReadImpl proteinMsaReadImpl;
	public ProteinMsaReadService() {
	}
	public ProteinMsaReadService(String fileLocation) {
		proteinMsaReadImpl = new ProteinMsaReadImpl(fileLocation);
	}
	

	public synchronized ProteinMsaReadImpl getChromoSomeReadImpl() {
		return proteinMsaReadImpl;
	}
	
	public synchronized  ArrayList<Sequence> getProteinSequences() {
		return proteinMsaReadImpl.getProteinSequences();
	}
	
	public synchronized void setProteinMsaReadImpl(ProteinMsaReadImpl proteinMsaReadImpl) {
		this.proteinMsaReadImpl = proteinMsaReadImpl;
	}
}
